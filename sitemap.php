<?php
/**
* Template Name: Sitemap
**/
get_header();
?>


<?php get_template_part('parts/components/component', 'hero');?>

<?php get_template_part('parts/components/component', 'breadcrumb');?>

<section class="component-centered-text">
	<div class="row align-center align-middle">
		<div class="col-12 col-lg-7 text-center lazy">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
			echo '<h1>' . get_the_title() . '</h1>';
				endwhile; else: ?>
			<?php endif; ?>
		</div>
	</div>
</section>

<div class="container sitemap">
	<section class="mainContent">
		<div class="row align-top align-center">
			<div class="col-12 col-md-12 col-lg-10 initialPost">
				<?php if ( has_nav_menu('Sitemap')):?>
	                <?php wp_nav_menu(array(
	                    'theme_location' => 'Sitemap',
	                    'walker' => new CleanMenuWalker(),
	                ));
	                ?>
	            <?php endif;?>
			</div>
		</div>
	</section>
</div>
<?php get_template_part('parts/templates/template', 'footer');?>
