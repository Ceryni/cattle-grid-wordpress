<?php
// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php');

// Custom Post Types
require_once(get_template_directory().'/assets/functions/custom-post-types.php');

// Taxonomies
require_once(get_template_directory().'/assets/functions/taxonomies.php');

// Customize the munu walker
require_once(get_template_directory().'/assets/functions/clean-walker.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php');

// Allow for numeric pagination
require_once(get_template_directory().'/assets/functions/pagination.php');

// Helper files
require_once(get_template_directory().'/assets/functions/helpers.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php');

// Related post function - no need to rely on plugins
require_once(get_template_directory().'/assets/functions/related-posts.php');

// Customize the WordPress login menu
require_once(get_template_directory().'/assets/functions/login.php');

// Customize the WordPress admin
require_once(get_template_directory().'/assets/functions/admin.php');

// Breadcrumbs Functions
require_once(get_template_directory().'/assets/functions/breadcrumb.php');

// Search Results Functions
require_once(get_template_directory().'/assets/functions/search.php');

// news filters
require_once(get_template_directory().'/assets/functions/news.php');

// styling the dashbaord
require_once(get_template_directory().'/assets/functions/dashboard.php');

// styling the acf
require_once(get_template_directory().'/assets/functions/acf.php');


// allow pages to have exceprts
add_post_type_support( 'page', 'excerpt' );

@ini_set( 'upload_max_size' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'max_execution_time', '300' );


add_filter( 'kdmfi_featured_images', function( $featured_images ) {
    $args = array(
        'id' => 'featured-icon',
        'desc' => 'Add an Icon to the page.',
        'label_name' => 'Featured Icon',
        'label_set' => 'Set featured icon',
        'label_remove' => 'Remove featured icon',
        'label_use' => 'Set featured icon',
        'post_type' => array( 'page' ),
    );

    $featured_images[] = $args;

    return $featured_images;
});

if ( SITECOOKIEPATH != COOKIEPATH ) {
setcookie(TEST_COOKIE, 'WP Cookie check', 0, SITECOOKIEPATH, COOKIE_DOMAIN);
}
