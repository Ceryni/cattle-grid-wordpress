<?php
	get_header();
?>
<section class="news">
	<div class="container">
		<div class="row align-middle align-center">
			<div class="col-12 col-md-12 col-lg-12 text-center">
				<h1><?php the_title();?></h1>
			</div>
		</div>
	</div>
</section>


<section class="component-centered-text">
	<div class="row align-center align-middle">
		<div class="col-12 col-lg-10 text-center lazy">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
			the_content();
				endwhile; else: ?>
			<?php endif; ?>
        </div>
    </div>
</section>

<?php get_template_part('parts/templates/template', 'footer');?>
