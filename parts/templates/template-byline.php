<?php
/**
 * The template part for displaying an author byline
 */

 $categories = get_the_category();
 $separator = ' ';
 $output = '';
 if ( ! empty( $categories ) ) {
	 foreach( $categories as $category ) {
		 $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
	 }
 }
?>
<p class="byline lazy">
	Posted <strong itemprop="datePublished"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ) . ' ago'; ?></strong>
</p>
