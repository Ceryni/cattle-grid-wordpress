<div class="shareLinks">
	<em>Share '<?php the_title(); ?>'</em><br/>
	<a rel="nofollow" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="button facebook"><span class="fa fa-facebook"></span></a>
	<a rel="nofollow" target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>" class="button twitter"><span class="fa fa-twitter"></span></a>
	<a rel="nofollow" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=<?php the_permalink(); ?>" class="button linkedin"><span class="fa fa-linkedin"></span></a>
	<a rel="nofollow" target="_blank" href="https://www.reddit.com/submit?url=<?php the_permalink(); ?>" class="button reddit"><span class="fa fa-reddit"></span></a>
	<a rel="nofollow" href="mailto:?&subject=Have you read '<?php the_title(); ?>'&body=I found this article here, take a look! <?php the_permalink(); ?>" class="button secondary"><span class="fa fa-envelope"></span></a>

</div>
