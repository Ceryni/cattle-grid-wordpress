<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('innerPost'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
    <section class="entry-content" itemprop="text">
		<div class="singleContent">
			<?php the_content(); ?>
			<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'holloway' ) . '</span> ', ', ', ''); ?></p>
		</div>
	</section> <!-- end article section -->
</article> <!-- end article -->
