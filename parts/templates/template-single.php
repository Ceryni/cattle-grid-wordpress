<?php
/**
* Template part for displaying a single post
*/

$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
$get_description = get_post(get_post_thumbnail_id())->post_excerpt;
$event_date = get_field('event_date');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('innerPost'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <div class="container">
        <div class="row align-top align-center">
            <div class="col-12 col-md-12 col-lg-10 text-center">
                <h2><?php the_title(); ?></h2>
                <?php /***
                <?php if ($image): ?>
                    <img class="SVGInject" src="<?php echo $image[0];?>" class="featured-image">
                <?php endif; ?>
                **/ ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row align-middle align-center">
            <div class="col-12 col-md-12 col-lg-10">
                <?php the_content();?>
            </div>
        </div>
    </div>
</article>
