</main>


<section class="module-aggregator">
	<div class="container">
		<div class="row align-middle align-center">
			<div class="col-12 col-md-12 col-lg-12 text-center">
					<p>
						Aggregator
					</p>
			</div>
		</div>
	</div>
</section>
<footer>
	<div class="container copyright">
		<div class="row align-top align-center">
			<div class="col-12 col-md-4 text-left logo">
					<p class="small">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
				<div class="col-12 col-md-1">
					&nbsp;
				</div>
				<div class="col-12 col-md-7">
					<?php if ( has_nav_menu('Footer Navigation')):?>
						<?php wp_nav_menu(array(
							'theme_location' => 'Footer Navigation',
							'walker' => new CleanMenuWalker(),
						));
						?>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html> <!-- end page -->
