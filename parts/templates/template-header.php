<?php /***
<?php get_template_part('parts/components/component', 'skippable');?>
**/ ?>
<div class="god-level">
    <header class="header">
        <div class="container">
            <div class="row align-center align-middle">
                <div class="col-12 col-md-5 text-right">
                    <?php get_template_part('parts/components/component', 'navigation-left');?>
                </div>
                <div class="col-12 col-md-2 text-left logo">
                    <a href="<?php echo home_url(); ?>"><img class="SVGInject" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/cg-logo-light.png" /></a>
                </div>
                <div class="col-12 col-md-5 text-right">
                    <?php get_template_part('parts/components/component', 'navigation-right');?>
                    <!-- mobile navigation -->
                    <div class="container mobile-menu-container">
                        <div class="row align-right mobile-nav">
                            <div class="col-12 col-lg-6 text-left navigate">
                                <a href="javascript:void(0);" class="toggleButton-js homeButton">
                                    <span class="fa fa-bars"></span>
                                </a>
                                <div class="toggle-js">
                                    <?php get_template_part('parts/components/component', 'navigation');?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>
