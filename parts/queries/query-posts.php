<?php
	$type = get_query_var( 'type' );
	$name = get_query_var( 'name' );
	$title = get_query_var( 'title' );
?>

<section class="component-article-posts">
	<div class="container">
		<?php if ($title): ?>
			<div class="row align-top align-center">
				<div class="col-12 col-md-12 col-lg-12 text-center">
					<h2 class="lazy"><?php echo $title; ?></h2>
				</div>
			</div>
		<?php endif; ?>

		<div class="row align-middle align-center">
			<div class="col-12 col-md-12 col-lg-12">
				<div class="sidebar-container">
					<ul>
						<?php
						$categories = get_terms( 'listing_categories', array('hide_empty' => true) );
							foreach($categories as $category) {
								echo '<li><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a> </li>';
							}
						 ?>
					</ul>
				</div>
				<div id="sidebar-select">
					<div class="row align-middle align-right">

						<div class="col-12 col-lg-12 text-right">
							<span>Filter by:</span> <select></select>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row align-middle align-center">
			<div class="col-12 col-md-12 col-lg-12">
				<div class="location-select">
					<ul>
						<?php
						$categories = get_terms( 'listing_locations', array('hide_empty' => true) );
							foreach($categories as $category) {
								echo '<li><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a> </li>';
							}
						 ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="row align-top align-center">
			<?php

				if ( get_query_var('paged') ) {
					$paged = get_query_var('paged');
				} elseif ( get_query_var('page') ) {
					$paged = get_query_var('page');
				} else {
					$paged = 1;
				}

			$args = array(
			    'posts_per_page' => -1,
			    'post_type'      => $type,
			    'hide_empty' => true,
				'paged' => $paged,
			);

			$wp_query = new WP_Query( $args );
			if ( $wp_query->have_posts() ) :
				while ( $wp_query->have_posts() ) : $wp_query->the_post();


				// get the image
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
				$content = get_the_content($post->ID );
				$post_date = get_the_date( 'F j, Y' );
				$author = get_the_author($post->ID);

				?>

				<div class="col-12 col-lg-4 query-post">
					<article>
						<div class="article-content lazy">
							<img class="SVGInject" src="<?php echo $image[0]; ?>" class="component-image lazy" />
							<div class="box flex">
								<h3><?php the_title();?></h3>
								<p itemprop="articleSection"><?php echo wp_trim_words( $content, 26); ?></p>
								<a href="<?php the_permalink();?>" class="button primary">Read More</a>
							</div>
						</div>
					</article>
				</div>

				<?php
			endwhile;
			wp_reset_query();
			endif;
			?>
		</div>
	</div>
</section>
