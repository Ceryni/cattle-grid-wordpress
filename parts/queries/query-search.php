<section class="search-result-query">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $content = get_the_content($post->ID );
    ?>
    <article itemscope itemtype="http://schema.org/Article">
        <div class="article-content text-left">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <img class="SVGInject result-image lazy" src="<?php echo $image[0]; ?>" class="component-image lazy" />
                    </div>
                    <div class="col-12 col-md-9">
                        <strong class="lazy"><?php the_title();?></strong>
                        <p itemprop="articleSection" class="lazy"><?php echo wp_trim_words( $content, 40); ?></p>
                        <a href="<?php the_permalink();?>" class="button primary lazy">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <?php endwhile; else: ?>
        <div class="row align-middle align-center">
            <div class="col-12 col-md-12 col-lg-12 no-result">
                <p class="lazy"> <img class="SVGInject" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/cross.svg" /> Sorry, but nothing matched your search terms. <br />Please try again with some different keywords.</p>
                <div class="lazy">
                    <?php get_template_part('parts/components/component', 'search');?>
                </div>
                <hr />
            </div>
        </div>
    <?php endif; ?>
</section>
