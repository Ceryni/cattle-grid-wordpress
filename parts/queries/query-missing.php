<?php if ( is_search() ) : ?>
	<p>Sorry, No Results. Try your search again</p>
	<div class="searchAgain lazy">
		<?php get_template_part('parts/search', 'form');?>
	</div>
<?php else: ?>
	<p class="lazy">Sorry! Post Not Found</p>
	<p class="lazy">Uh Oh. Something is missing. Try double checking things</p>
	<div class="searchAgain">
		<?php get_template_part('parts/search', 'form');?>
	</div>
<?php endif; ?>
