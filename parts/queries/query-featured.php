<?php
    $args = array(
        'posts_per_page' => 1,
        'post_type'      => $postype->name,
        'hide_empty' 	 => true,
        'meta_key' 		 => 'featured_story',
        'meta_value'    => 1,
    );
    $wp_query = new WP_Query( $args );
    if ( $wp_query->have_posts() ) :
        while ( $wp_query->have_posts() ) : $wp_query->the_post();
        // get the image
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $content = get_the_content($post->ID );
        $content = apply_filters('the_content', $content);
        $postype = get_sub_field('select_content_type');
    ?>

    <section class="component-featured-post" style="background-image: url('<?php echo $image[0]; ?>')">
        <div class="container">
            <div class="row align-right align-top">
                <div class="col-12 col-md-5 align-self-middle align-center">
                    <article>
                        <div class="article-content text-center">
                            <h3 class="lazy">Featured Story</h3>
                            <p itemprop="articleSection" class="lazy"><?php echo wp_trim_words( $content, 60); ?></p>
                            <a href="<?php the_permalink();?>" class="button primary lazy">Read More</a>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>

    <?php
endwhile;
wp_reset_query();
endif;
wp_reset_postdata();
?>
