<?php
// Check value exists.
if( have_rows('home_modules') ):
	// Loop through rows.
	while ( have_rows('home_modules') ) : the_row();?>
	<?php if( get_row_layout() == 'module_8_4' ):?>
		<div class="container">
			<div class="row align-middle align-center">

				<?php if( have_rows("cards") ): ?>
					<?php while( have_rows("cards") ): the_row(); ?>

						<?php if( have_rows("card_large") ): ?>
							<?php while( have_rows("card_large") ): the_row();
								$icon = get_sub_field("icon");
								$title = get_sub_field("title");
								$summary = get_sub_field("summary");
								$background_colour = get_sub_field("background_colour");
							?>
								<div class="col-12 col-md-8">
									<div class="card card-<?php echo $background_colour; ?>">
										<img src="<?php echo $icon ?>" />
										<h2><?php echo $title;?></h2>
										<p><?php echo $summary; ?></p>
										<p></p>
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>

						<?php if( have_rows("card_small") ): ?>
							<?php while( have_rows("card_small") ): the_row();
								$icon = get_sub_field("icon");
								$title = get_sub_field("title");
								$summary = get_sub_field("summary");
								$background_colour = get_sub_field("background_colour");
							?>
								<div class="col-12 col-md-4">
									<div class="card card-<?php echo $background_colour; ?>">
										<img src="<?php echo $icon ?>" />
										<h2><?php echo $title;?></h2>
										<p><?php echo $summary; ?></p>
										<p></p>
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>

					<?php endwhile; ?>
				<?php endif; ?>

			</div>
		</div>
	<?php endif;?>

	<?php if( get_row_layout() == 'module_6_6' ):
		$image_left = get_sub_field('image_left');
		$image_right = get_sub_field('image_right');
	?>
		<div class="container">
			<div class="row align-middle align-center">
				<?php if( have_rows("card_left") ): ?>
					<?php while( have_rows("card_left") ): the_row();
						$icon = get_sub_field("icon");
						$title = get_sub_field("title");
						$summary = get_sub_field("summary");
						$background_colour = get_sub_field("background_colour");
					?>
						<div class="col-12 col-md-6">
							<div class="card card-<?php echo $background_colour; ?>">
								<img src="<?php echo $icon ?>" />
								<h2><?php echo $title;?></h2>
								<p><?php echo $summary; ?></p>
								<p></p>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>

				<?php if( have_rows("card_right") ): ?>
					<?php while( have_rows("card_right") ): the_row();
						$icon = get_sub_field("icon");
						$title = get_sub_field("title");
						$summary = get_sub_field("summary");
						$background_colour = get_sub_field("background_colour");
					?>
						<div class="col-12 col-md-6">
							<div class="card card-<?php echo $background_colour; ?>">
								<img src="<?php echo $icon ?>" />
								<h2><?php echo $title;?></h2>
								<p><?php echo $summary; ?></p>
								<p></p>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>

	<?php endif;?>

	<?php if( get_row_layout() == 'module_4_8_quad' ):

		$image_small = get_sub_field('image_small');
		?>

		<div class="container">
			<div class="row align-middle align-center">
				<div class="col-12 col-md-4">
					<?php
						$featured = get_sub_field('featured');
					?>
					<div class="card">
						<?php echo $featured; ?>
					</div>
				</div>
				<div class="col-12 col-md-8">
					<?php if( have_rows("quad_boxes") ): ?>
						<?php while( have_rows("quad_boxes") ): the_row(); ?>

						<div class="container">
							<div class="row align-middle align-center">
								<?php for ($i = 0; $i < 4; $i++):?>
									<?php if( have_rows("card_ '. $i .'") ): ?>
    									<?php while( have_rows("card_ '. $i .'") ): the_row();

    										$icon = get_sub_field("icon");
    										$title = get_sub_field("title");
    										$summary = get_sub_field("summary");
    										$background_colour = get_sub_field("background_colour");
    									?>
    									<div class="col-12 col-md-6">
											<div class="card card-<?php echo $background_colour; ?>">
												<img src="<?php echo $icon; ?>" />
												<h2><?php echo $title;?></h2>
												<p><?php echo $summary; ?></p>
												<p></p>
											</div>
    									</div>
    									<?php endwhile; ?>
    								<?php endif; ?>
								<?php endfor; ?>
							</div>
						</div>

						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>

	<?php endif;?>

	<?php if( get_row_layout() == 'module_aggregator' ):?>
		<div class="container">
			<div class="row align-middle align-center">
				<div class="col-12 col-md-12 col-lg-12">
					aggregator
				</div>
			</div>
		</div>
	<?php endif;?>

	<?php
	// End loop.
endwhile;

// No value.
else :
	// Do something...
endif;
?>
