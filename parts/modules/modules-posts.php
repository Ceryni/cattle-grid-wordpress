
<section class"component-text-left-image-right">
	<img class="svg-bg svg-bg-left svg-bg-left-image" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bgs/DYSIS Background Lines - Left-Right Aligned Image - Thick.svg">
	<div class="container">
		<?php if( have_rows('text_left_image_right') ): ?>
			<?php while( have_rows('text_left_image_right') ): the_row();
			// Get sub field values.
			$title = get_sub_field('title');
			$content = get_sub_field('content');
			$image = get_sub_field('image');
			$button_text = get_sub_field('button_text');
			$button_link = get_sub_field('button_link');
			?>
			<div class="row align-center align-top">
				<div class="col-12 col-lg-5 align-self-middle">
					<?php if ($title): ?>
						<h2 class="lazy"><?php echo $title ?></h2>
					<?php endif; ?>
					<?php if ($content): ?>
						<div class="lazy">
							<?php echo $content; ?>
						</div>
					<?php endif; ?>
					<?php if ($button_text && $button_link): ?>
						<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
					<?php endif; ?>
				</div>
				<?php if ($image): ?>
					<div class="col-12 col-lg-5 text-center">
						<img class="SVGInject" src="<?php echo $image; ?>" class="component-image lazy" />
					</div>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
</section>

<?php
$text_center = get_field('text_center');
if ($text_center):?>
<section class="component-centered-text" class="white">
	<div class="container">
		<div class="row align-center align-middle">
			<div class="col-12 col-lg-10 text-center">
				<div class="lazy">
					<?php echo $text_center; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>



<?php if( have_rows('media') ):?>
	<?php while( have_rows('media') ): the_row();?>
		<section class="component-single-media">
			<div class="container">
				<div class="row align-center align-middle">
					<div class="col-12 col-lg-6 text-center">
						<?php
						$type = get_sub_field('image_or_video');
						// uploaded
						$image = get_sub_field('image');
						// video location
						$location = get_sub_field('uploaded_or_external');
						// uploaded
						$video_uploaded = get_sub_field('uploaded');
						// caption for media
						$caption = get_sub_field('caption');
						// external
						if($location === 'external') {
							$iframe = get_sub_field('external');
							// use preg_match to find iframe src
							preg_match('/src="(.+?)"/', $iframe, $matches);

							$src = $matches[1];
							// add extra params to iframe src
							$params = array(
								'controls'    => 0,
								'hd'        => 1,
								'autohide'    => 1
							);
							$new_src = add_query_arg($params, $src);
							$iframe = str_replace($src, $new_src, $iframe);
							// add extra attributes to iframe html
							$attributes = 'frameborder="0"';
							$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
						}
						?>
						<?php if($type === 'video'):?>
							<?php if($location === 'uploaded'):?>
								<div class="video-cover fullWidth lazy">
									<video class="heroVideo" src="<?php echo $video_uploaded; ?>"></video>
									<button class="playPause"><svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52 100" style="enable-background:new 0 0 52 100;" xml:space="preserve"><polygon style="fill:#41B6E6;" points="0,-0.5 0,99.5 51.9,47.6 "/></svg></button>
								</div>
							<?php endif;?>
							<?php if($location === 'embed'):?>
								<div class="video-cover fullWidth lazy">
									<?php echo $iframe;?>
								</div>
							<?php endif;?>
						<?php elseif($type === 'image'):?>
						<?php endif;?>
						<div class="caption lazy">
							<p><?php echo $caption ?></p>
						</div>
					</div>

				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif;?>



<section class="component-cross-links">
	<div class="container">
		<div class="row align-middle align-center">
			<?php if( have_rows('cross_links') ): ?>
				<?php while( have_rows('cross_links') ): the_row();?>
					<?php if( have_rows('card_1') ): ?>
						<?php while( have_rows('card_1') ): the_row();
						$title = get_sub_field('title');
						$image = get_sub_field('image');
						$button_text = get_sub_field('button_text');
						$button_link = get_sub_field('button_link');
						?>
						<div class="col-12 col-md-6 col-lg-6">
							<div class="cross-link-box align-middle">
								<div class="cross-link-box-inner" <?php if ($image): ?>style="background-image: url('<?php echo $image; ?>');"<?php endif; ?>>
									<div class="row align-middle align-left">
										<div class="col-12 col-md-7">
											<?php if ($title): ?>
												<span class="lazy"><?php echo $title; ?></span>
											<?php endif; ?>
											<?php if ($button_text && $button_link): ?>
												<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>

				<?php if( have_rows('card_2') ): ?>
					<?php while( have_rows('card_2') ): the_row();
						$title = get_sub_field('title');
						$image = get_sub_field('image');
						$button_text = get_sub_field('button_text');
						$button_link = get_sub_field('button_link');
					?>
					<div class="col-12 col-md-6 col-lg-6">
						<div class="cross-link-box align-middle">
							<div class="cross-link-box-inner" <?php if ($image): ?>style="background-image: url('<?php echo $image; ?>');"<?php endif; ?>>
								<div class="row align-middle align-left">
									<div class="col-12 col-md-7">
										<?php if ($title): ?>
											<span class="lazy"><?php echo $title; ?></span>
										<?php endif; ?>
										<?php if ($button_text && $button_link): ?>
											<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>

		<?php endwhile; ?>
	<?php endif; ?>
</div>
