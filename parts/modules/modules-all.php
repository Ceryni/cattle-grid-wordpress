<?php

// Check value exists.
if( have_rows('modules') ):

	// Loop through rows.
	while ( have_rows('modules') ) : the_row();?>

	<?php if( get_row_layout() == 'module_tabs' ):?>
		<section class="component-tabs">
			<div class="container">
				<div class="row align-middle align-center">
					<div class="col-12 col-md-12 col-lg-12 vertical-tabs custom-tabs">
						<?php if( have_rows("tabs") ):
							$counter = 0;
							?>
							<ul class="tabs tab-text">
								<?php while( have_rows("tabs") ): the_row();
								// vars
								$tab_name = get_sub_field("tab_name");
								$tab_icon = get_sub_field("tab_icon");
								$colours = array(
									1 => 'primary',
									2 => 'tertiary',
									3 => 'quaternary',
									4 => 'senary',
									5 => 'secondary',
									6 => 'quintary',
									7 => 'default',
								);

								if($counter === 7) {
									$counter = 1;
								} else {
									$counter++;
								}

								?>
								<?php if ($tab_name):?>
									<li class="tab-page-slug lazy <?php echo $colours[$counter]; ?>" slug="<?php echo strtolower(str_replace(' ', '_', $tab_name)); ?>">
										<?php if ($tab_icon): ?>
											<img class="SVGInject" src="<?php echo $tab_icon; ?>" class="tab-icon lazy" />
										<?php endif; ?>
										<?php echo $tab_name; ?>
									</li>
								<?php endif; ?>

							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					<div class="tab-content-holder">
						<?php if( have_rows("tabs") ):
							$counter = 0;
							?>
							<ul class="tabs">
								<?php while( have_rows("tabs") ): the_row();
								$content = get_sub_field("content");
								$content = apply_filters('the_content', $content);
								$tab_name = get_sub_field("tab_name");
								$image = get_sub_field("image");
								$widget = get_sub_field("add_widget_script");
								$script = get_sub_field("script");
								$two_cols = get_sub_field("two_columns");
								$fullwidth = get_sub_field("full_width");

								$query = get_sub_field("query");
								$type = get_sub_field("query_type");

								if($two_cols){
									$col1 = get_sub_field("column_1");
									$col1 = apply_filters('the_content', $col1);
									$col2 = get_sub_field("column_2");
									$col2 = apply_filters('the_content', $col2);
								}
								$colours = array(
									1 => 'primary',
									2 => 'tertiary',
									3 => 'quaternary',
									4 => 'senary',
									5 => 'secondary',
									6 => 'quintary',
									7 => 'default',
								);

								$col = 5;

								if($counter === 7) {
									$counter = 1;
								} else {
									$counter++;
								}

								if ($widget || $two_cols || $fullwidth) {
									$col = 11;
								}
								?>
								<div class="tab-content <?php echo $colours[$counter]; ?>" slug="<?php echo strtolower(str_replace(' ', '_', $tab_name)); ?>">
									<div class="container">
										<div class="row collapse">
											<div class="col-12 col-md-12 zero">
												<div class="tab-content-inner text-left">
													<div class="tab-content-inner-colour text-center">
														<div class="container" style="width: 100%;">
															<div class="row align-top align-center">
																	<?php if ($query): ?>

																		<?php
																			if($type->name === 'policies') {
																				get_template_part('parts/queries/query', 'policy-static');
																			} elseif ($type->name === 'reports') {
																				get_template_part('parts/queries/query', 'reports');
																			}
																			?>

																	<?php else: ?>

																	<div class="col-12 col-md-<?php echo $col; ?> text-left">
																		<div class="lazy">
																			<?php if (!$two_cols || $fullwidth): ?>
																				<?php if ($content): ?>
																					<?php echo $content; ?>
																				<?php endif; ?>
																			<?php endif; ?>
																			<?php if ($two_cols): ?>
																				<div class="row align-top align-center">
																					<?php if ($col1): ?>
																						<div class="col-12 col-md-6">
																							<?php echo $col1 ?>
																						</div>
																					<?php endif; ?>
																					<?php if ($col2): ?>
																						<div class="col-12 col-md-6">
																							<?php echo $col2 ?>
																						</div>
																					<?php endif; ?>
																				</div>

																			<?php endif; ?>
																			<?php if ($widget && $script): ?>
																				<?php echo $script; ?>
																				<style>
																					div.cqc-widget {
																						width: 49% !important;
																						display: inline-block !important;
																					}
																				</style>
																			<?php endif; ?>
																		</div>
																	</div>
																	<?php if (!$fullwidth || !$widget || !$two_cols): ?>
																		<?php if ($image): ?>
																			<div class="col-12 col-md-5">
																				<img class="SVGInject" src="<?php echo $image; ?>" class="lazy">
																			</div>
																		<?php endif; ?>
																	<?php endif; ?>
																<?php endif; ?>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_tabs_with_query' ):?>
	<section class="component-tabs-query">
		<div class="container">
			<div class="row align-middle align-center">
				<div class="col-12 col-md-12 col-lg-12 vertical-tabs custom-tabs">
					<?php if( have_rows("tabs_query") ):
						$counter = 0;
						?>
						<ul class="tabs">
							<?php while( have_rows("tabs_query") ): the_row();
							// vars
							$tab_name = get_sub_field("tab_name");
							$tab_icon = get_sub_field("tab_icon");
							$colours = array(
								1 => 'primary',
								2 => 'tertiary',
								3 => 'quaternary',
								4 => 'senary',
								5 => 'secondary',
								6 => 'quintary',
								7 => 'default',
							);

							if($counter === 7) {
								$counter = 1;
							} else {
								$counter++;
							}
							?>
							<li class="tab-page-slug lazy <?php echo $colours[$counter]; ?>" slug="<?php echo strtolower(str_replace(' ', '_', $tab_name)); ?>">
								<?php if ($tab_icon): ?>
									<img class="SVGInject" src="<?php echo $tab_icon; ?>" class="tab-icon lazy" />
								<?php endif; ?>
								<?php echo $tab_name; ?>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
				<div class="tab-content-holder">
					<?php if( have_rows("tabs_query") ):
						$counter = 0;
						?>
						<ul class="tabs">
							<?php while( have_rows("tabs_query") ): the_row();
							$content = get_sub_field("content");
							$tab_name = get_sub_field("tab_name");
							$category  = get_sub_field("category");
							$category_accessory = get_sub_field("category_accessory");
							$category_team = get_sub_field("category_team");
							$summary = get_sub_field("summary");
							$colours = array(
								1 => 'primary',
								2 => 'tertiary',
								3 => 'quaternary',
								4 => 'senary',
								5 => 'secondary',
								6 => 'quintary',
								7 => 'default',
							);

							if($counter === 7) {
								$counter = 1;
							} else {
								$counter++;
							}
							// set the post type
							set_query_var( 'type', $content->name );
							?>
							<div class="tab-content  <?php echo $colours[$counter]; ?>" slug="<?php echo strtolower(str_replace(' ', '_', $tab_name)); ?>">
								<div class="container">
									<div class="row collapse">
										<div class="col-12 col-md-12 zero">
											<div class="tab-content-inner text-left">
												<div class="tab-content-inner-colour text-center">
													<div class="container">
														<div class="row align-middle align-center">
															<div class="col-12 col-md-12 col-lg-9 tab-summary">
																<?php if ($summary): ?>
																	<div class="lazy">
																		<?php echo $summary; ?>
																	</div>
																<?php endif; ?>
															</div>
														</div>
														<?php
														if ($category_team) {
															set_query_var( 'category', $category_team->slug);
															get_template_part('parts/queries/query', 'company');
														}
														else {
															get_template_part('parts/queries/query', 'posts');
														}

														?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_overlayed_image' ):?>
	<section class="component-text-left-image-right-overlay">
		<?php if( have_rows('overlayed_image') ): ?>
			<?php while( have_rows('overlayed_image') ): the_row();
			// Get sub field values.
			$title = get_sub_field('title');
			$content = get_sub_field('content');
			$content = apply_filters('the_content', $content);
			$image = get_sub_field('image');
			$button_text = get_sub_field('button_text');
			$button_link = get_sub_field('button_link');
			?>
			<div>
				<img class="svg-bg svg-bg-left svg-bg-left-image bg-half" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bgs/DYSIS Background Lines - Feature Link - Half.svg">
				<div class="box">
					<article class="sm-text-center">
						<?php if ($title): ?>
							<h2 class="lazy"><?php echo $title; ?></h2>
						<?php endif; ?>
						<?php if ($content): ?>
							<div class="lazy">
								<?php echo $content; ?>
							</div>
						<?php endif; ?>
						<?php if ($button_text): ?>
							<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
						<?php endif; ?>
					</article>
				</div>
			</div>
			<div>
				<?php if ($image): ?>
					<div class="bgImage" style="background-image: url('<?php echo $image; ?>')">&nbsp;</div>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_one_column' ): ?>
	<section class="component-one-col-text-card">
		<?php if( have_rows('one_column') ): ?>
			<?php while( have_rows('one_column') ): the_row();?>
				<div class="container full">
					<div class="row align-middle align-center">
						<?php if( have_rows('column_1') ): ?>
							<?php while( have_rows('column_1') ): the_row();
							$image = get_sub_field('image');
							?>
							<?php if ($image): ?>
								<div class="bgImage bg1" style="background-image: url('<?php echo $image; ?>')"></div>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif;?>

					<div class="container">
						<div class="row align-middle align-center">
							<div class="col-12 col-md-12 text-left">
								<?php if( have_rows('column_1') ):
									while( have_rows('column_1') ): the_row();
									$title = get_sub_field('title');
									$image = get_sub_field('image');
									$button_text = get_sub_field('button_text');
									$button_link = get_sub_field('button_link');
									$content = get_sub_field('content');
									$content = apply_filters('the_content', $content);
									?>
									<article>
										<div class="summaryHolder">
											<?php if ($title): ?>
												<h2 class="lazy"><?php echo $title; ?></h2>
											<?php endif; ?>
											<?php if ($content): ?>
												<div class="lazy">
													<?php echo $content; ?>
												</div>
											<?php endif; ?>
											<?php if ($button_link && $button_text): ?>
												<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
											<?php endif; ?>
										</div>
									</article>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_two_column' ): ?>
	<section class="component-two-col-text-card">
		<?php if( have_rows('two_column') ): ?>
			<?php while( have_rows('two_column') ): the_row();?>

				<div class="container full">
					<div class="row align-middle align-center">
						<?php if( have_rows('column_1') ): ?>
							<?php while( have_rows('column_1') ): the_row();
							$image = get_sub_field('image');
							?>
							<?php if ($image): ?>
								<div class="bgImage bg1" style="background-image: url('<?php echo $image; ?>')"></div>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif;?>

					<?php if( have_rows('column_2') ): ?>
						<?php while( have_rows('column_2') ): the_row();
						$image = get_sub_field('image');
						?>
						<?php if ($image): ?>
							<div class="bgImage bg2" style="background-image: url('<?php echo $image; ?>')"></div>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif;?>

				<div class="container">
					<div class="row align-middle align-center">
						<div class="col-12 col-md-12 col-lg-6 text-left">
							<?php if( have_rows('column_1') ):
								while( have_rows('column_1') ): the_row();
								$title = get_sub_field('title');
								$image = get_sub_field('image');
								$button_text = get_sub_field('button_text');
								$button_link = get_sub_field('button_link');
								$content = get_sub_field('content');
								$content = apply_filters('the_content', $content);
								?>
								<article>
									<div class="summaryHolder">
										<?php if ($title): ?>
											<h2 class="lazy"><?php echo $title; ?></h2>
										<?php endif; ?>
										<?php if ($content): ?>
											<div class="lazy">
												<?php echo $content; ?>
											</div>
										<?php endif; ?>
										<?php if ($button_link && $button_text): ?>
											<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
										<?php endif; ?>
									</div>
								</article>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>

					<div class="col-12 col-md-12 col-lg-6 text-right">
						<?php if( have_rows('column_2') ):
							while( have_rows('column_2') ): the_row();
							$title = get_sub_field('title');
							$image = get_sub_field('image');
							$button_text = get_sub_field('button_text');
							$button_link = get_sub_field('button_link');
							$content = get_sub_field('content');
							$content = apply_filters('the_content', $content);
							?>
							<article>
								<div class="summaryHolder">
									<?php if ($title): ?>
										<h2 class="lazy"><?php echo $title; ?></h2>
									<?php endif; ?>
									<?php if ($content): ?>
										<div class="lazy">
											<?php echo $content; ?>
										</div>
									<?php endif; ?>
									<?php if ($button_link && $button_text): ?>
										<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
									<?php endif; ?>
								</div>
							</article>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; ?>
<?php endif; ?>
</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_testimonials' ): ?>
	<?php if(get_sub_field('testimonial_carousel')):?>
		<?php get_template_part('parts/components/component', 'carousel-testimonial');?>
	<?php endif; ?>
<?php endif;?>

<?php if( get_row_layout() == 'module_cross_links' ): ?>
	<section class="component-cross-links">
		<div class="container">
			<div class="row align-middle align-center">
				<?php if( have_rows('cross_links') ): ?>
					<?php while( have_rows('cross_links') ): the_row();?>
						<?php if( have_rows('card_1') ): ?>
							<?php while( have_rows('card_1') ): the_row();
							$title = get_sub_field('title');
							$image = get_sub_field('image');
							$button_text = get_sub_field('button_text');
							$button_link = get_sub_field('button_link');
							?>
							<div class="col-12 col-md-6 col-lg-6">
								<div class="cross-link-box align-middle">
									<div class="cross-link-box-inner" <?php if ($image): ?>style="background-image: url('<?php echo $image; ?>');"<?php endif; ?>>
										<div class="row align-middle align-left">
											<div class="col-7">
												<?php if ($title): ?>
													<span class="lazy"><?php echo $title; ?></span>
												<?php endif; ?>
												<?php if ($button_text && $button_link): ?>
													<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
												<?php endif; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>

					<?php if( have_rows('card_2') ): ?>
						<?php while( have_rows('card_2') ): the_row();
						$title = get_sub_field('title');
						$image = get_sub_field('image');
						$button_text = get_sub_field('button_text');
						$button_link = get_sub_field('button_link');
						?>
						<div class="col-12 col-md-6 col-lg-6">
							<div class="cross-link-box align-middle">
								<div class="cross-link-box-inner" <?php if ($image): ?>style="background-image: url('<?php echo $image; ?>');"<?php endif; ?>>
									<div class="row align-middle align-left">
										<div class="col-7">
											<?php if ($title): ?>
												<span class="lazy"><?php echo $title; ?></span>
											<?php endif; ?>
											<?php if ($button_text && $button_link): ?>
												<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>

			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_cross_links_three' ): ?>
	<section class="component-cross-links">
		<div class="container">
			<div class="row align-middle align-center">
				<?php if( have_rows('cross_links') ): ?>
					<?php while( have_rows('cross_links') ): the_row();?>
						<?php if( have_rows('card_1') ): ?>
							<?php while( have_rows('card_1') ): the_row();
							$title = get_sub_field('title');
							$image = get_sub_field('image');
							$button_text = get_sub_field('button_text');
							$button_link = get_sub_field('button_link');
							?>
							<div class="col-12 col-md-4">
								<div class="cross-link-box align-middle">
									<div class="cross-link-box-inner" <?php if ($image): ?>style="background-image: url('<?php echo $image; ?>');"<?php endif; ?>>
										<div class="row align-middle align-left">
											<div class="col-8">
												<?php if ($title): ?>
													<span class="lazy"><?php echo $title; ?></span>
												<?php endif; ?>
												<?php if ($button_text && $button_link): ?>
													<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
												<?php endif; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>

					<?php if( have_rows('card_2') ): ?>
						<?php while( have_rows('card_2') ): the_row();
						$title = get_sub_field('title');
						$image = get_sub_field('image');
						$button_text = get_sub_field('button_text');
						$button_link = get_sub_field('button_link');
						?>
						<div class="col-12 col-md-4">
							<div class="cross-link-box align-middle">
								<div class="cross-link-box-inner" <?php if ($image): ?>style="background-image: url('<?php echo $image; ?>');"<?php endif; ?>>
									<div class="row align-middle align-left">
										<div class="col-8">
											<?php if ($title): ?>
												<span class="lazy"><?php echo $title; ?></span>
											<?php endif; ?>
											<?php if ($button_text && $button_link): ?>
												<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>

				<?php if( have_rows('card_3') ): ?>
					<?php while( have_rows('card_3') ): the_row();
					$title = get_sub_field('title');
					$image = get_sub_field('image');
					$button_text = get_sub_field('button_text');
					$button_link = get_sub_field('button_link');
					?>
					<div class="col-12 col-md-4">
						<div class="cross-link-box align-middle">
							<div class="cross-link-box-inner" <?php if ($image): ?>style="background-image: url('<?php echo $image; ?>');"<?php endif; ?>>
								<div class="row align-middle align-left">
									<div class="col-8">
										<?php if ($title): ?>
											<span class="lazy"><?php echo $title; ?></span>
										<?php endif; ?>
										<?php if ($button_text && $button_link): ?>
											<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
</div>
</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_text_left_image_right' ): ?>
	<?php if( have_rows('text_left_image_right') ): ?>
		<?php while( have_rows('text_left_image_right') ): the_row();
		// Get sub field values.
		$title = get_sub_field('title');
		$content = get_sub_field('content');
		$content = apply_filters('the_content', $content);
		$image = get_sub_field('image');
		$button_text = get_sub_field('button_text');
		$button_link = get_sub_field('button_link');
		$bg = get_sub_field('background_colour');
		?>
		<section class="component-text-left-image-right hasCircle-js <?php echo $bg; ?>" style="background-image: url('<?php echo $image; ?>')">
			<img src="<?php echo $image; ?>" class="sm-show">
			<div class="container">
				<div class="holder">
					<div class="box"></div>
					<div class="row align-left align-top">
						<div class="col-12 col-md-6 align-self-middle sm-text-center order-bottom">
							<div class="circle"></div>

							<?php if ($title): ?>
								<h2><?php echo $title ?></h2>
							<?php endif; ?>
							<?php if ($content): ?>
								<?php echo $content; ?>
							<?php endif; ?>
							<?php if ($button_text && $button_link): ?>
								<a href="<?php echo $button_link ?>" class="button primary"><?php echo $button_text ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>

	<?php endwhile; ?>
<?php endif; ?>
<?php endif;?>

<?php if( get_row_layout() == 'module_text_right_image_left' ): ?>
	<?php if( have_rows('text_right_image_left') ): ?>
		<?php while( have_rows('text_right_image_left') ): the_row();
		// Get sub field values.
		$title = get_sub_field('title');
		$content = get_sub_field('content');
		$content = apply_filters('the_content', $content);
		$image = get_sub_field('image');
		$button_text = get_sub_field('button_text');
		$button_link = get_sub_field('button_link');
		$bg = get_sub_field('background_colour');
		?>
		<section class="component-text-right-image-left hasCircle-js <?php echo $bg; ?>" style="background-image: url('<?php echo $image; ?>')">
			<img src="<?php echo $image; ?>" class="sm-show">
			<div class="container">
				<div class="holder">
					<div class="box"></div>
					<div class="row align-right align-top">
						<div class="col-12 col-md-6 align-self-middle sm-text-center order-bottom">
							<div class="circle"></div>

							<?php if ($title): ?>
								<h2><?php echo $title ?></h2>
							<?php endif; ?>
							<?php if ($content): ?>
								<?php echo $content; ?>
							<?php endif; ?>
							<?php if ($button_text && $button_link): ?>
								<a href="<?php echo $button_link ?>" class="button primary"><?php echo $button_text ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>
<?php endif;?>


<?php if( get_row_layout() == 'module_post_query' ): ?>
	<section class="component-queries">
		<div class="container">

			<?php
				$title = get_sub_field("title");
                $carousel = get_sub_field("carousel");?>
				<?php if( have_rows("post_query") ): ?>
					<?php while( have_rows("post_query") ): the_row();
						$content = get_sub_field("content");
                        if ($carousel) {
							set_query_var( 'type', $content->name );
							get_template_part('parts/queries/query', 'posts-carousel');
						} else {
							set_query_var( 'type', $content->name );
							get_template_part('parts/queries/query', 'posts');
						}
					endwhile;?>
				<?php endif; ?>
		</div>
	</section>
<?php endif;?>



<?php if( get_row_layout() == 'module_article_cards' ): ?>
	<section class="component-queries">
		<div class="container">

			<?php
			$title = get_sub_field("title");
			$show_buttons = get_sub_field("show_buttons");


			if( have_rows("card_query") ):
				while( have_rows("card_query") ): the_row();

				$colours = array(
					1 => 'primary',
					2 => 'tertiary',
					3 => 'quaternary',
					4 => 'senary',
					5 => 'secondary',
					6 => 'quintary',
					7 => 'default',
				);

				$counter = 0; ?>

				<div class="row align-top align-center">

					<?php if ($title): ?>
						<div class="col-12 col-md-12 col-lg-12 text-center">
							<h2 class="lazy"><?php echo $title; ?></h2>
						</div>
					<?php endif; ?>

					<?php

					$show_title =  get_sub_field('show_title_of_page');
					$show_buttons =  $show_buttons;
					$text_bg = get_sub_field('text_background');
					$position = get_sub_field('title_top_or_bottom_or_image');
					$title_order = 'order: 2';
					$image_order = 'order: 1';

					if ($position === 'top') {
						$title_order = 'order: 1';
						$image_order = 'order: 2';

					}
					if ($position === 'bottom') {
						$title_order = 'order: 2';
						$image_order = 'order: 1';
					}

					$rows = get_sub_field('card_content');

					if( $rows ):
						foreach( $rows as $row ):

							if($counter === 7) {
								$counter = 1;
							} else {
								$counter++;
							}

							$image = $row['image'];
							$title  = $row['title'];
							$content = $row['content'];
							$click_through = $row['click_through'];
							$link = $row['link'];
						?>

							<div class="col-12 col-lg-4 query-page text-center <?php if($text_bg):?>text-bg <?php endif;?> <?php echo $colours[$counter]; ?>">
								<article>
									<div class="article-content text-center lazy">
										<?php if ($image): ?>
											<img class="SVGInject" style="<?php echo $image_order; ?>" src="<?php echo $image; ?>" />
										<?php endif; ?>
										<?php if ($show_title): ?>
											<h3 class="" style="<?php echo $title_order; ?>"><?php echo esc_html( $title ); ?></h3>
										<?php endif; ?>
										<div class="summary">
											<div class="summary-toggle-js">
												<div class="shorter active">
													<p>
														<?php echo wp_trim_words( $content, 15); ?>
													</p>
												</div>
												<div class="longer hidden">
													<?php echo $content; ?>
												</div>
											</div>
											<?php if ($click_through && $link): ?>
												<a href="<?php echo $link; ?>" class="button">Read More</a>
											<?php else: ?>
												<a href="javascript:void(0)" class="button toggleButton-js closed">Read More</a>
											<?php endif;?>
										</div>

									</div>
								</article>
							</div>

						<?php endforeach; ?>
					<?php endif;?>

				</div>
			<?php endwhile;?>
		<?php endif; ?>


	</div>
</section>
<?php endif;?>




<?php if( get_row_layout() == 'module_carousel' ): ?>
	<?php $gallery = get_sub_field('gallery_carousel');
	if( $gallery ): ?>
	<section class="component-single-media">
		<img class="svg-bg svg-bg-left svg-bg-left-image" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bgs/DYSIS Background Lines - Centre Image.svg">
		<div class="container">
			<div class="row align-middle align-center">
				<div class="col-12 col-md-7 text-center">
					<section class="carousel">
						<div class="owl-carousel owl-carousel-gallery">
							<?php foreach( $gallery as $image ):
								$info = pathinfo($image['url']);?>
								<div>
									<?php if ($info["extension"] == "mp4"):?>
										<div class="video-cover fullWidth lazy">
											<video class="heroVideo" src="<?php echo esc_url($image['url']); ?>"></video>
											<button class="playPause"><svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52 100" style="enable-background:new 0 0 52 100;" xml:space="preserve"><polygon style="fill:#216079;" points="0,-0.5 0,99.5 51.9,47.6 "/></svg></button>
										</div>
									<?php endif; ?>

									<?php if ($info["extension"] != "mp4"):?>
										<img class="SVGInject" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="lazy"/>
									<?php endif; ?>
									<p class="lazy"><?php echo esc_html($image['caption']); ?></p>
								</div>
							<?php endforeach; ?>
						</div>
					</section>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
<?php endif;?>

<?php if( get_row_layout() == 'module_centered_text_with_title' ): ?>
	<section class="component-centered-text-with-title">
		<?php
		$title = get_sub_field('title');
		$content = get_sub_field('content');
		$content = apply_filters('the_content', $content);
		?>
		<div class="container">
			<div class="row align-middle align-center">
				<div class="col-12 col-md-12 col-lg-12 text-center">
					<article>
						<?php if ($title): ?>
							<h2 class="lazy"><?php echo $title; ?></h2>
						<?php endif; ?>
						<?php if ($content): ?>
							<div class="lazy">
								<?php echo $content; ?>
							</div>
						<?php endif; ?>
					</article>
				</div>
			</div>
		</div>
	</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_image_centered_text_with_title' ): ?>
	<section class="component-image-centered-text-with-title">
		<?php
		$image = get_sub_field('image');
		$title = get_sub_field('title');
		$content = get_sub_field('content');
		$content = apply_filters('the_content', $content);
		?>
		<div class="container">
			<div class="row align-middle align-center">
				<div class="col-12 col-md-12 col-lg-12 text-center">
					<article>
						<?php if ($image): ?>
							<img class="SVGInject" src="<?php echo $image; ?>" class="lazy" />
						<?php endif; ?>
						<?php if ($title): ?>
							<h2 class="lazy"><?php echo $title; ?></h2>
						<?php endif; ?>
						<?php if ($content): ?>
							<div class="lazy">
								<?php echo $content; ?>
							</div>
						<?php endif; ?>
					</article>
				</div>
			</div>
		</div>
	</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_form' ): ?>
	<section class="component-form">
		<div class="container">
			<div class="row align-center align-top">
				<?php if(get_sub_field('select_form') === 'business'):?>
					<?php acfe_form('listing-form'); ?>
				<?php endif; ?>
				<?php if(get_sub_field('select_form') === 'edit'):?>
					<?php acfe_form('edit_listings'); ?>	   
				<?php endif; ?>

			</div>
		</div>
	</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_icons' ): ?>
	<?php $title = get_sub_field("title"); ?>
	<section class="component-icons">
		<div class="container">
			<?php if ($title): ?>
				<div class="row align-middle align-center">
					<div class="col-12 col-md-12 col-lg-12 text-center">
						<h2 class="lazy"><?php echo $title; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row align-top align-center">
				<?php if( have_rows("icons") ): ?>
					<?php while( have_rows("icons") ): the_row();
					// vars
					$image = get_sub_field("image");
					$title = get_sub_field("title");
					$content = get_sub_field("content");
					$content = apply_filters('the_content', $content);
					$link = get_sub_field("link");
					$button_link = get_sub_field('button_link');
					$button_text = get_sub_field('button_text');
					?>
					<div class="col-12 col-md-4 text-center">
						<?php if ($image && $link): ?>
							<a href="<?php echo $link; ?>"><img class="SVGInject" src="<?php echo $image; ?>" /></a>
						<?php else: ?>
							<img class="SVGInject" src="<?php echo $image; ?>" />
						<?php endif; ?>
						<?php if ($title): ?>
							<h5><?php echo $title; ?></h5>
						<?php endif; ?>
						<?php if ($content): ?>
							<?php echo $content; ?>
						<?php else: ?>
							<?php echo $content; ?>
						<?php endif; ?>
						<?php if ($button_link && $button_text): ?>
							<a href="<?php echo $button_link; ?>" class="button primary hollow lazy"><?php echo $button_text; ?></a>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif;?>

<?php if( get_row_layout() == 'modules_media' ): ?>
	<section class="component-single-media">
		<img class="svg-bg svg-bg-left svg-bg-left-image" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bgs/DYSIS Background Lines - Centre Image.svg">
		<?php
		$title = get_sub_field('title');
		$content = get_sub_field('content');
		$content = apply_filters('the_content', $content);
		$order_of_content = get_sub_field('order_of_content');
		$button_link = get_sub_field('button_link');
		$button_text = get_sub_field('button_text');
		$image_order = '';
		$text_order = '';
		if ($order_of_content === 'bottom') {
			$image_order = 2;
			$text_order = 1;
		}
		if ($order_of_content === 'top') {
			$image_order = 1;
			$text_order = 2;
		}
		?>
		<div class="container">
			<div class="row align-center align-middle">
				<div class="col-12 col-lg-7 text-center" style="order: <?php echo $text_order; ?>">
					<?php if ($title): ?>
						<h2 class="lazy"><?php echo $title; ?></h2>
					<?php endif; ?>
					<?php if ($content): ?>
						<div class="lazy">
							<?php echo $content; ?>
						</div>
					<?php endif; ?>
					<?php if ($button_link && $button_text): ?>
						<a href="<?php echo $button_link; ?>" class="button primary hollow lazy"><?php echo $button_text; ?></a>
					<?php endif; ?>
				</div>

				<?php $gallery = get_sub_field('gallery_carousel');
				if( $gallery ): ?>

				<div class="col-12 col-md-7 text-center"  style="order: <?php echo $image_order; ?>">
					<section class="carousel" class="component-single-media">
						<div class="owl-carousel owl-carousel-gallery">
							<?php foreach( $gallery as $image ):
								$info = pathinfo($image['url']);?>
								<div>
									<?php if ($info["extension"] == "mp4"):?>
										<div class="video-cover fullWidth lazy">
											<video class="heroVideo" src="<?php echo esc_url($image['url']); ?>"></video>
											<button class="playPause"><svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52 100" style="enable-background:new 0 0 52 100;" xml:space="preserve"><polygon style="fill:#216079;" points="0,-0.5 0,99.5 51.9,47.6 "/></svg></button>
										</div>
									<?php endif; ?>
									<?php if ($info["extension"] != "mp4"):?>
										<img class="SVGInject" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>"  class="lazy"/>
									<?php endif; ?>
									<p class="lazy"><?php echo esc_html($image['caption']); ?></p>
								</div>
							<?php endforeach; ?>
						</div>
					</section>
				</div>
			<?php endif; ?>
		</section>
	<?php endif;?>


	<?php if( get_row_layout() == 'module_contact_cards' ): ?>
		<section class="component-contact-card">
			<div class="container">

				<?php if( have_rows('contact_card_us') ): ?>
					<?php while( have_rows('contact_card_us') ): the_row();

					// Get sub field values.
					$location = get_sub_field('location');
					$address = get_sub_field('address');
					$contact_number = get_sub_field('contact_number');
					$contact_email = get_sub_field('contact_email');
					$map = get_sub_field('map');

					?>
					<div class="row align-center align-top">
						<div class="col-12 col-lg-5">
							<?php if ($location): ?>
								<h3 class="lazy"><?php echo $location; ?></h3>
							<?php endif; ?>
							<?php if ($address): ?>
								<address class="lazy">
									<?php echo $address; ?>
								</address>
							<?php endif; ?>
							<ul>
								<?php if ($contact_number): ?>
									<li class="lazy">
										<a href="tel:<?php echo $contact_number; ?>" class="align-center">
											<img class="SVGInject" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Phone-icon.svg" class="component-icon" /> <?php echo $contact_number; ?>
										</a>
									</li>
								<?php endif; ?>
								<?php if ($contact_email): ?>
									<li class="lazy">
										<a href="mailto:<?php echo $contact_email; ?>" class="align-center">
											<img class="SVGInject" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Email-icon.svg" class="component-icon" /> <?php echo $contact_email; ?>
										</a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
						<?php if ($map): ?>
							<div class="col-12 col-lg-5">
								<img class="SVGInject" src="<?php echo $map; ?>" class="component-map lazy">
							</div>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>

			<?php if( have_rows('contact_card_uk') ): ?>
				<?php while( have_rows('contact_card_uk') ): the_row();

				// Get sub field values.
				$location = get_sub_field('location');
				$address = get_sub_field('address');
				$contact_number = get_sub_field('contact_number');
				$contact_email = get_sub_field('contact_email');
				$map = get_sub_field('map');

				?>
				<div class="row align-center align-top">
					<div class="col-12 col-lg-5">
						<?php if ($location): ?>
							<h3 class="lazy"><?php echo $location; ?></h3>
						<?php endif; ?>
						<?php if ($address): ?>
							<address class="lazy">
								<?php echo $address; ?>
							</address>
						<?php endif; ?>
						<ul>
							<?php if ($contact_number): ?>
								<li class="lazy">
									<a href="tel:<?php echo $contact_number; ?>" class="align-center">
										<img class="SVGInject" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Phone-icon.svg" class="component-icon" /> <?php echo $contact_number; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($contact_email): ?>
								<li class="lazy">
									<a href="mailto:<?php echo $contact_email; ?>" class="align-center">
										<img class="SVGInject" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Email-icon.svg" class="component-icon" /> <?php echo $contact_email; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
					<?php if ($map): ?>
						<div class="col-12 col-lg-5">
							<img class="SVGInject" src="<?php echo $map; ?>" class="component-map lazy">
						</div>
					<?php endif; ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_standard_text' ): ?>
	<?php $content = get_sub_field("content"); ?>
	<?php if ($content): ?>
		<section class="component_standard_text">
			<div class="container">
				<div class="row align-middle align-center">
					<div class="col-12 col-md-12 col-lg-8 text-center standard-text-container">
						<div class="lazy">
							<?php echo $content; ?>
							<?php if( have_rows("buttons") ): ?>
			                    <?php while( have_rows("buttons") ): the_row();
			                        // vars
			                        $button_text = get_sub_field("button_text");
			                        $button_link = get_sub_field("button_link");
			                    ?>
			                        <a href="<?php echo $button_link; ?>" class="button"><?php echo $button_text ?></a>
			                    <?php endwhile; ?>
			            <?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
<?php endif;?>

<?php if( get_row_layout() == 'module_standard_text_full_width' ): ?>
	<?php $content = get_sub_field("content"); ?>
	<?php if ($content): ?>
		<section class="component_standard_text standard_full_width">
			<div class="container">
				<div class="row align-middle align-center">
					<div class="col-12 col-md-12 text-center standard-text-container">
						<div class="lazy">
							<?php echo $content; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
<?php endif;?>

<?php if( get_row_layout() == 'module_centered-text-box-bg' ): ?>
	<?php if( have_rows('centered-text-box-bg') ): ?>
		<?php while( have_rows('centered-text-box-bg') ): the_row();
		$title = get_sub_field('title');
		$content = get_sub_field('content');
		$content = apply_filters('the_content', $content);
		$image = get_sub_field('image');
		$button_text = get_sub_field('button_text');
		$button_link = get_sub_field('button_link');
		?>
		<section class="component-centered-text-box-bg" style="background-image: url('<?php echo $image; ?>')">
			<div class="row align-center align-middle">
				<div class="col-12 col-lg-7 text-center">
					<div class="summaryHolder">
						<?php if ($title): ?>
							<h2 class="lazy"><?php echo $title; ?></h2>
						<?php endif; ?>
						<?php if ($content): ?>
							<div class="lazy">
								<?php echo $content; ?>
							</div>
						<?php endif; ?>
						<?php if ($button_text && $button_link): ?>
							<a href="<?php echo $button_link ?>" class="button primary hollow lazy"><?php echo $button_text ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>
<?php endif;?>

<?php if( get_row_layout() == 'module_map_location' ): ?>
	<section class="component-map-location">
		<div class="container">
			<div class="row align-middle align-center">
				<div class="col-12 col-md-12 col-lg-12 text-center">
					<h2>Where we're based</h2>
				</div>
			</div>
			<div class="row align-top align-center">

				<div class="col-12 col-md-6 custom-tabs horizontal-tabs">
					<div class="tab-content-holder">
						<?php if( have_rows("location") ): ?>
							<div class="tabs">
								<?php while( have_rows("location") ): the_row();
									$name = get_sub_field('name');
									$map_image = get_sub_field('map_image');
									$post_code = get_sub_field('post_code');
								?>
								<div class="tab-content" slug="<?php echo strtolower(str_replace(' ', '_', $post_code)); ?>">
									<div class="container">
										<div class="row collapse">
											<div class="col-12 col-md-12 zero">
												<img class="SVGInject" src="<?php echo $map_image;?> ">
											</div>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-12 col-md-6 custom-tabs horizontal-tabs">
					<?php if( have_rows("location") ): ?>
						<ul class="tabs">
							<?php while( have_rows("location") ): the_row();
								// vars
								$name = get_sub_field('name');
								$office_type = get_sub_field('office_type');
								$street_address = get_sub_field('street_address');
								$city = get_sub_field('city');
								$county = get_sub_field('county');
								$post_code = get_sub_field('post_code');
								$country = get_sub_field('country');
								$link = get_sub_field('link');
								$link_text = get_sub_field('link_text');
								if ($link) {
									$link_url = $link['url'];
									$link_target = $link['target'] ? $link['target'] : '_self';
								}
							?>
							<?php if ($name): ?>
								<li class="tab-page-slug lazy" slug="<?php echo strtolower(str_replace(' ', '_', $post_code)); ?>">
									<img class="SVGInject" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/location-blue.svg" />
									<address>
										<span itemprop="legalName"><?php echo $name; ?></span><br />
										<?php if ($office_type): ?>
											<?php echo $office_type; ?> <br />
										<?php endif; ?>
										<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
											<?php if ($street_address): ?>
												<span itemprop="streetAddress"><?php echo $street_address; ?></span>
											<?php endif; ?>
											<?php if ($city): ?>
												<span itemprop="addressLocality"><?php echo $city; ?></span>
											<?php endif; ?>
											<?php if ($county): ?>
												<span itemprop="addressRegion"><?php echo $county; ?></span>
											<?php endif; ?>
											<?php if ($post_code): ?>
												<span itemprop="postalCode"><?php echo $post_code; ?></span>
											<?php endif; ?>
											<?php if ($country): ?>
												<span itemprop="addressCountry"><?php echo $country; ?></span>
											<?php endif; ?>
										</div>
										<br />
										<?php if ($link_text && $link): ?>
											<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo $link_text ?></a>
										<?php endif; ?>
									</address>
								</li>
							<?php endif; ?>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>
</section>
<?php endif;?>

<?php if( get_row_layout() == 'module_featured_post' ): ?>
	<?php get_template_part('parts/queries/query', 'featured'); ?>
<?php endif;?>

<?php if( get_row_layout() == 'module_service_map' ): ?>
	<?php $map = get_sub_field("show_map"); ?>
	<?php if ($map): ?>
		<?php get_template_part('parts/queries/query', 'service-map'); ?>
	<?php endif;?>
<?php endif;?>


<?php if( get_row_layout() == 'module_middle_link' ):
	// Get sub field values.
	$banner_image = get_sub_field('banner_image');
	$title = get_sub_field('title');
	$button_text = get_sub_field('button_text');
	$button_link = get_sub_field('button_link');
?>
	<section class="module-direct_link" style="background-image: url('<?php echo $banner_image; ?>')">
		<div class="container">
			<div class="row align-middle align-center">
				<div class="col-12 col-md-12 col-lg-7 text-center">
					<div class="box">
						<h3 class="lazy" ><?php echo $title ?></h3>
						<?php if ($button_link && $button_text): ?>
							<a href="<?php echo $button_link; ?>" class="button primary lazy"><?php echo $button_text; ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php endif;?>

<?php
// End loop.
endwhile;

// No value.
else :
	// Do something...
endif;
?>
