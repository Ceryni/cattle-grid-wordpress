<?php if( have_rows('carousel') ):?>
<section class="carousel">
	<div class="owl-carousel owl-carousel-home">
		<?php while( have_rows('carousel') ): the_row();
			$type = get_sub_field('image_or_video');
			// uploaded
			$image = get_sub_field('image');
			// video location
			$location = get_sub_field('uploaded_or_external');
			// uploaded
			$video_uploaded = get_sub_field('uploaded');
			// title
			$title = get_sub_field('title');
			// content
			$content = get_sub_field('content');
			// button_text
			$button_text = get_sub_field('button_text');
			// button_link
			$button_link = get_sub_field('button_link');
			// external
			if($location === 'external') {
				$iframe = get_sub_field('external');
				// use preg_match to find iframe src
				preg_match('/src="(.+?)"/', $iframe, $matches);

				$src = $matches[1];
				// add extra params to iframe src
				$params = array(
					'controls'    => 0,
					'hd'        => 1,
					'autohide'    => 1
				);
				$new_src = add_query_arg($params, $src);
				$iframe = str_replace($src, $new_src, $iframe);
				// add extra attributes to iframe html
				$attributes = 'frameborder="0"';
				$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
			}
			?>
			<?php if($type === 'video'):?>
				<?php if($location === 'uploaded'):?>
					<div>
						<div class="video-cover fullWidth">
							<video class="heroVideo" src="<?php echo $video_uploaded; ?>"></video>
							<button class="playPause"><svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52 100" style="enable-background:new 0 0 52 100;" xml:space="preserve"><polygon style="fill:#41B6E6;" points="0,-0.5 0,99.5 51.9,47.6 "/></svg></button>
						</div>
						<div class="carousel-holder">
							<?php get_template_part('parts/components/component', 'carousel-inside');?>
						</div>
					</div>
				<?php endif;?>
				<?php if($location === 'embed'):?>
					<div>
						<div class="video-cover fullWidth">
							<?php echo $iframe;?>
						</div>
						<div class="carousel-holder">
							<?php get_template_part('parts/components/component', 'carousel-inside');?>
						</div>
					</div>
				<?php endif;?>
			<?php elseif($type === 'image'):?>
				<div>
					<img src="<?php echo $image; ?>"/>
					<div class="carousel-holder">
						<?php get_template_part('parts/components/component', 'carousel-inside');?>
					</div>
				</div>
			<?php endif;?>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>
