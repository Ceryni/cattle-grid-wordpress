<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label>
		<input type="search" class="search-field" autocomplete="none" value="<?php echo get_search_query() ?>" name="s" title="Search for" />
	</label>
	<button type="submit" class="button tertiary"><i class="fa fa-search"></i></button>
</form>
