<?php
	// Can remove the code 404 hero code?
	if(is_404()) {
		if( have_rows('404_page', 'options') ):
		    while( have_rows('404_page', 'options') ): the_row();
			   $image = get_sub_field('hero_image', 'options');
		    endwhile;
		endif;
	} elseif( is_search() || is_page_template('search.php')) {
		if( have_rows('search_page', 'options') ):
		    while( have_rows('search_page', 'options') ): the_row();
			   $image = get_sub_field('hero_banner_search');
		    endwhile;
		endif;
	} else {
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		$image = $image[0];
		//$image = $image;
	}
?>

<section class="component-hero" class="hero" style="background-image: url('<?php echo $image; ?>')">

</section>
