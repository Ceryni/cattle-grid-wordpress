<section class="component-navigation">
    <nav>
        <div class="menu">
            <?php if( is_user_logged_in()):?>
                <?php if ( has_nav_menu('Menu Right Logged In')):?>
                    <?php wp_nav_menu(array(
                        'theme_location' => 'Menu Right Logged In',
                        'walker' => new CleanMenuWalker(),
                    ));
                    ?>
                <?php endif;?>

            <?php else:?>
                <?php if ( has_nav_menu('Menu Right')):?>
                    <?php wp_nav_menu(array(
                        'theme_location' => 'Menu Right',
                        'walker' => new CleanMenuWalker(),
                    ));
                    ?>
                <?php endif;?>

            <?php endif;?>

        </div>
    </nav>
</section>
