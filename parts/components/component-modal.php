<div class="modal successModal-php" modal="success">
    <span class="fa fa-close modal-close"></span>
    <div class="modalHold">
        <div class="container">
            <div class="row align-middle align-center">
                <div class="col-12 col-md-12 col-lg-12 text-center">
                    <h2 class="tertiary">Thankyou for your enquiry</h2>
                    <p>
                        A member of our team will be in touch as soon as possible to discuss further.
                    </p>
                    <a href="javascript:void();" class="button secondary modal-close">Okay, Thanks</a>
                </div>
            </div>
        </div>
    </div>
</div>
