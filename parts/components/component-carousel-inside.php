<?php
    // title
    $title = get_sub_field('title');
    // content
    $content = get_sub_field('content');
    // button_text
    $button_text = get_sub_field('button_text');
    // button_link
    $button_link = get_sub_field('button_link');
?>

<div class="container carousel-inside">
    <div class="row align-middle align-left">
        <div class="col-12 col-md-12 col-lg-4 zero">
            <div class="box primary">
                <?php if ($title): ?>
                    <h1><?php echo $title; ?></h1>
                <?php endif; ?>
                <?php if ($content): ?>
                    <p><?php echo $content; ?></p>
                <?php endif; ?>
                <?php if( have_rows("buttons") ): ?>
                    <?php while( have_rows("buttons") ): the_row();
                        // vars
                        $button_text = get_sub_field("button_text");
                        $button_link = get_sub_field("button_link");
                    ?>
                        <a href="<?php echo $button_link; ?>" class="button"><?php echo $button_text ?></a>
                    <?php endwhile; ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>
