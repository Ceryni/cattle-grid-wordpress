<section class="component-navigation">
    <nav>
        <div class="menu text-left">
            <?php if ( has_nav_menu('Menu Left')):?>
                <?php wp_nav_menu(array(
                    'theme_location' => 'Menu Left',
                    'walker' => new CleanMenuWalker(),
                ));
                ?>
            <?php endif;?>
        </div>
    </nav>
</section>
