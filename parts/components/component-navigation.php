<section class="component-navigation">
    <nav>
        <div class="menu">
            <?php if ( has_nav_menu('Main')):?>
                <?php wp_nav_menu(array(
                    'theme_location' => 'Main',
                    'walker' => new CleanMenuWalker(),
                ));
                ?>
            <?php endif;?>
        </div>
    </nav>
</section>
