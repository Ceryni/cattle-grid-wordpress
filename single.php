<?php
	get_header();
	$cpt = get_post_type(get_the_ID());
?>
<?php get_template_part('parts/components/component', 'hero');?>

<?php get_template_part('parts/components/component', 'breadcrumb');?>


<section class="news-post">
	<div class="container">
		<div class="row align-center">
			<div class="col-12 col-md-12 col-lg-10">
				<?php get_template_part('parts/templates/template', 'single');?>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('parts/templates/template', 'footer');?>
