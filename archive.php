<?php
	/**
	* Template Name: Archive
	**/
	get_header();

	$cpt = 'posts';
	if(get_post_type(get_the_ID()) !== 'page') {
		$cpt = get_post_type(get_the_ID());
	}
	get_template_part('parts/components/component', 'breadcrumb');
?>


<section class="<?php echo $cpt;?>-container">
	<div class="container">
		<div class="row align-center align-top">
			<div class="col-12 col-md-12 col-lg-9">
				<?php
					set_query_var('postsPerPage', -1);
				   	get_template_part('parts/queries/query', $cpt);
				?>
			</div>
		</div>
	</div>
</section>
<?php get_template_part('parts/templates/template', 'footer');?>
