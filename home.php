<?php
/**
* Template Name:  Home
*/
get_header();

?>

<?php get_template_part('parts/components/component', 'carousel');?>
<?php get_template_part('parts/modules/modules', 'all');?>
<?php get_template_part('parts/templates/template', 'footer');?>
