<?php
	get_header();
?>

<?php get_template_part('parts/components/component', 'hero');?>

<?php get_template_part('parts/components/component', 'breadcrumb');?>

<section class="component-centered-text">
	<div class="row align-center align-middle">
		<div class="col-12 col-lg-7 text-center lazy">
			<h1>Search Results</h1>
		</div>
	</div>
</section>

<div class="container">
	<section class="mainContent">
		<div class="row align-top align-center searchResults">

			<div class="col-12 text-left searchResults-title">
				<p class="lazy"> <img class="SVGInject" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/search.svg" /> Search Results for "<strong><?php echo get_search_query();?></strong>"</p>
			</div>

			<div class="col-12 searchQuery">
				<?php get_template_part('parts/queries/query', 'search');?>
			</div>
		</div>
	</section>
</div>


<?php get_template_part('parts/templates/template', 'footer');?>
