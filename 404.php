<?php
	get_header();
?>
<?php get_template_part('parts/components/component', 'hero');?>

<?php get_template_part('parts/components/component', 'breadcrumb');?>

<section class="fullWidth adapt normal">
	<div class="container inner starter">
		<div class="row align-middle align-center">
			<div class="col-12 col-md-9 text-center lazy error-container">
				<?php if( have_rows('404_page', 'options') ): ?>
					<?php while( have_rows('404_page', 'options') ): the_row(); ?>
						<h1><?php the_sub_field('heading'); ?></h1>
						<img class="SVGInject" src="<?php the_sub_field('image'); ?>" alt="404 Error icon">

						<h2><?php the_sub_field('sub_heading'); ?></h2>

						<?php the_sub_field('caption'); ?>

						<a class="button primary" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text') ?></a>

					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('parts/templates/template', 'footer');?>
