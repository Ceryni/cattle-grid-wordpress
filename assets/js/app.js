// Library
import owlCarousel from 'owl.carousel';
import SVGInjector from 'svg-injector';
import Cookies from 'js-cookie';

// Entry points
import Base from './Base/Base';
import Mobile from './Base/Mobile';
import Accessibility from './Base/Accessibility';

// Define lib Constructors
window.SVGInjector = SVGInjector;
window.Cookies = Cookies;

// Begin the scripts
$(document).ready(() => {
    // check to see if we've loaded
    console.log('%c The main JS has been found and loaded!', 'background: #dd9623; color: #222222; padding: 5px; border-radius: 10px; font-weight: bold');
    // Load the base core classes
    Base.initialise();
    Mobile.initialise();
    Accessibility.initialise();
});
