// Entry points
import Admin from './Admin/Admin';

// Begin the scripts
jQuery(document).ready(() => {
    // check to see if we've loaded
    console.log('%c The main JS has been found and loaded!', 'background: #dd9623; color: #222222; padding: 5px; border-radius: 10px; font-weight: bold');
    // Load the base core classes
    Admin.initialise();
});
