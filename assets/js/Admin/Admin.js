/**
 * Creates a new class Admin
 * @class Represents Admin.
 */
class Admin {
    /**
     * Initialises all delegates
     */
    initialise() {
        // show the file is loaded
        this.base();
    }

    /**
     *  Check if the file is loaded
     */
    base() {
        console.log('%c Admin JS has been found and loaded!', 'background: #03A9F4; color: #222222; padding: 5px; border-radius: 10px; font-weight: bold');
    }
}

export default new Admin();
