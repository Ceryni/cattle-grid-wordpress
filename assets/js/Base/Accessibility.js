/**
 * Creates a new class Base
 * @class Represents Base.
 */
class Accessibility {
    /**
     * Initialises all delegates
     */
    initialise() {
        // show the file is loaded
        this.base();
        this.loadAccessibility();
    }

    base() {
        console.log('%c Accessibility JS has been found and loaded!', 'background: #03A9F4; color: #222222; padding: 5px; border-radius: 10px; font-weight: bold');
    }

    // click to load a colour theme class to the body
    loadAccessibility() {
        // all themes
        let themes = ['black-white-mode', 'black-yellow-mode', 'yellow-black-mode', 'dark-mode', 'bright-mode'];

        // all fonts
        let fonts = ['standard', 'medium', 'large'];

        // quickly define the body
        let body = $('body');

        // map all themes
        let allThemes = themes.map((theme) => {
            // on click of the button with the data name attrib
            $(`a[data-name="${theme}"]`).on('click', (event) => {
                // set the cookie
                Cookies.set('theme', theme);
                // grab the attrib
                let attrib = 'data-theme';
                // update the attrib
                body.attr(attrib, body.attr(attrib) == theme ? '' : theme);
                // if no attrib is defined or set
                if (body.attr(attrib) === '' || body.attr(attrib) === undefined) {
                    // removed the stored cookie
                    Cookies.remove('theme');
                }
                // stop the bubble
                event.stopPropagation();
            });
        });

        // map all of the fonts
        let allFonts = fonts.map((font) => {
            // on click of the button with the data name attrib
            $(`a[data-size="${font}"]`).on('click', (event) => {
                // set the cookie
                Cookies.set('font', font);
                // grab the attrib
                let attrib = 'data-font';
                // update the attrib
                body.attr(attrib, body.attr(attrib) == font ? '' : font);
                // if no attrib is defined or set
                if (body.attr(attrib) === '' || body.attr(attrib) === undefined) {
                    // removed the stored cookie
                    Cookies.remove('font');
                }
                // stop the bubble
                event.stopPropagation();
            });
        });
    }
}

export default new Accessibility();
