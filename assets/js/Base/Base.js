/**
 * Creates a new class Base
 * @class Represents Base.
 */
class Base {
    /**
     * Initialises all delegates
     */
    initialise() {
        this.base();
        this.loadHelpers();
        this.loadSVGInject();
        this.loadOwlCarousel();
        this.loadModal();
        this.loadToggle();
        this.loadTabs();
        this.loadPlayPause();
        this.loadLazyLoad();
        this.loadSelectConvert();
    }

    /**
     *  Check if the file is loaded
     */
    base() {
        console.log('%c Base JS has been found and loaded!', 'background: #03A9F4; color: #222222; padding: 5px; border-radius: 10px; font-weight: bold');
    }

    /**
     * Fuction to allow replacing all instances of a search item
     * @param {String} search
     * @param {String} replacement
     * @returns {Object} - Contains all new replaced search results
     */
    loadHelpers() {
        String.prototype.replaceAll = (search, replacement) => {
            const target = this;
            return target.replace(new RegExp(search, 'g'), replacement);
        };
    }

    /**
     * SVG Inject library to convert SVG img paths to full blown SVG code
     */
    loadSVGInject() {
        // Elements to inject
        const mySVGsToInject = document.querySelectorAll('.SVGInject');
        // just target every image
        // if it's not an SVG it will ignore it anyway
        $('img').each(function () {
            $(this).addClass('SVGInject');
        });

        SVGInjector(mySVGsToInject);

        // Do the injection
        setTimeout(() => {
            SVGInjector(mySVGsToInject);
        }, 1000);
    }

    /**
     *  Load play and pause buttons for custom video files
     */
    loadPlayPause() {
        // define the video
        let videoHolder = $('.video-cover');
        let ppbutton = $('.playPause');

        // if it has a button
        if (ppbutton) {
            // Event listener for the play/pause button
            ppbutton.on('click', ({ currentTarget }) => {
                let clicked = $(currentTarget);
                console.log(clicked.parent('.video-cover').find('video'));
                let newVideo = clicked.parent('.video-cover').find('video');
                // if it's already paused
                if (newVideo.get(0).paused) {
                    // Play the videoHolderNew
                    newVideo.trigger('play');
                    // add a class
                    clicked.addClass('paused');
                    // Update the button text to 'Pause'
                    clicked.html("<svg version='1.1'xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 52 100' style='enable-background:new 0 0 52 100;' xml:space='preserve'><g><rect style='fill:#216079;' width='17.3' height='100'/><rect x='34.7' style='fill:#216079;' width='17.3' height='100'/></g></svg>");
                } else {
                    // Pause the videoHolderNew
                    newVideo.trigger('pause');
                    clicked.removeClass('paused');
                    // Update the button text to 'Play'
                    clicked.html("<svg version='1.1'  xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 52 100' style='enable-background:new 0 0 52 100;' xml:space='preserve'><polygon style='fill:#216079;' points='0,-0.5 0,99.5 51.9,47.6 '/></svg>");
                }
            });
        }
    }

    /**
     * Owl Carousel Library
     * NPM installed
     */
    loadOwlCarousel() {
        // the carousel container
        const carousel = $('.owl-carousel.owl-carousel-home');
        // if a carousel is found on the page
        if (carousel[0]) {
            // create an owl carousel
            carousel.owlCarousel({
                nav: false, // Show next and prev buttons
                items: 1,
                dots: true,
                autoPlay: true,
                autoplaySpeed: 3000, //  speed switching between slides
                dragEndSpeed: 1000,
                singleItem: true,
                // animateIn: 'fadeIn',
                // animateOut: 'fadeOut',
                pagination: false,
                autoplay: true,
                slideSpeed: 7000,
                autoplayTimeout: 8000,
                autoplayHoverPause: false,
                loop: true,
                navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            });
        }

        const news = $('.owl-carousel.owl-carousel-news');
        // if a carousel is found on the page
        if (news[0]) {
            // create an owl carousel
            news.owlCarousel({
                nav: true, // Show next and prev buttons
                items: 3,
                dots: true,
                autoPlay: true,
                autoplaySpeed: 3000, //  speed switching between slides
                dragEndSpeed: 1000,
                // animateIn: 'fadeIn',
                // animateOut: 'fadeOut',
                pagination: true,
                autoplay: true,
                slideSpeed: 7000,
                autoplayTimeout: 8000,
                autoplayHoverPause: false,
                loop: true,
                navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                responsive: {
                    0: {
                        items: 1,
                    },
                    768: {
                        items: 2,
                    },
                    1170: {
                        items: 3,
                    },
                },
            });
        }
    }

    /**
     * Click to see modal
     */
    loadModal() {
        // register vars
        let modalButton = $('.modalOpen');
        let modal = $('.modal');
        let close = $('.modal-close');
        let attrib = 'modal';
        let bodyClass = 'modal-js';

        // on modal click
        modalButton.on('click', ({ currentTarget }) => {
            // add a class to the body
            $('body').addClass(bodyClass);

            // get current attrib
            let currentAttrib = $(currentTarget).attr(attrib);
            modal
                .filter(function () {
                    // of the attrib matches the the current active data attrib
                    // add a class of 'active'
                    return $(this).attr(attrib) === currentAttrib;
                })
                .addClass('active');
        });
        // on close button click
        close.on('click', ({ currentTarget }) => {
            // remove class from the modal
            $(currentTarget).closest(modal).removeClass('active');
            // remove class from the body
            $('body').removeClass(bodyClass);
        });
    }

    /**
     * Load toggle for menu opening
     */
    loadToggle() {
        // define the button class
        let theButton = $('.toggleButton-js');
        // define the toggle container class
        let toggleContainer = $('.toggle-js');
        // on click,
        theButton.on('click', function () {
            // are we the mobile button?
            if ($(this).hasClass('homeButton')) {
                // if so, add a class to the body
                $('body').toggleClass('menuOpen-js');
            }
            if ($(this).hasClass('fa-search')) {
                // if so, add a class to the body
                $('body').toggleClass('menuOpen-js-shop');
            }

            // switch icon
            $(this).find('span').toggleClass('fa-bars fa-close');

            // on any other button instance
            theButton
                .not(this)
                // remove the active class
                .removeClass('active')
                // and on the following container
                .next(toggleContainer)
                // remove that active too
                .removeClass('active');

            //apply an active class to the button clicked
            $(this)
                .toggleClass('active')
                // the next toggle Container
                .next(toggleContainer)
                // toggle class active
                .toggleClass('active');
        });
    }

    /**
     *  Custom tabs
     */
    loadTabs() {
        // holds all tabs's
        let tabs = $('.custom-tabs');
        // define the button class
        let theButton = $('.tab-page-slug');
        // button Height
        let buttonHeight = $('.tab-page-slug').height();
        // define the toggle container class
        let toggleContainer = $('.tab-content');
        // data attrib name
        let attrib = 'slug';

        // add active to the first button
        theButton.first().addClass('active');
        // add active to first tab content container
        $('.tab-content:not(.mobile-show)').first().addClass('active');

        // if the element exists
        if (tabs.length) {
            // on click,
            theButton.on('click', (event) => {
                // current
                let clicked = $(event.currentTarget);
                // get data attrib of current active tab
                let currentClass = clicked.attr(attrib);
                // remove the active class from all other buttons
                if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    theButton.removeClass('active');
                    //apply an active class to the button clicked
                    clicked.addClass('active');
                }
                // rmeove all active classes on the tab containers
                toggleContainer.removeClass('active');
                // filter all tab containers
                toggleContainer
                    .filter(function () {
                        // of the attrib matches the the current active data attrib
                        // add a class of 'active'
                        return $(this).attr(attrib) === currentClass;
                    })
                    .addClass('active');

                // for mobile only
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    // expand tabs
                    tabs.toggleClass('expand');
                    console.log(clicked);
                    // if the target has a class of active
                    clicked.toggleClass('active').siblings().removeClass('active');
                }
                event.preventDefault();
            });
        }
    }

    /**
     * Parallax scrolling on CMS added imagery
     * Catch different window heights and widths
     */
    loadParallax() {
        $(() => {
            const $el = $('.bgImage');
            $(window).on('scroll', () => {
                const IS_IPAD = navigator.userAgent.match(/iPad/i) != null;
                const HEAD = $('.bgImage');
                let scroll = $(document).scrollTop();

                let window_height = $(window).height();
                let window_width = $(window).width();

                if (window_height > 1250 || IS_IPAD) {
                    scroll = scroll / 24 - 150;
                } else {
                    scroll = scroll / 21;
                }

                $el.css({
                    'background-position': `0% ${scroll}px`,
                });

                if (HEAD) {
                    scroll = scroll / 20 + 120;
                    HEAD.css({
                        'background-position': `20% ${scroll}px`,
                    });
                }

                if (IS_IPAD) {
                    $el.css({
                        'background-position': `20% ${scroll}px`,
                    });
                }
            });

            const $elPost = $('body.single .bgImage');
            $(window).on('scroll', () => {
                let scroll = $(document).scrollTop();
                scroll = scroll / 21;
                $elPost.css({
                    'background-position': `0% ${scroll}px`,
                });
            });
        });
    }

    /**
     * Lazy load elements into view from top to _bottom
     * Can also load from left
     */
    loadLazyLoad() {
        $.fn.offScreen = function (distance) {
            const $t = $(this),
                $w = $(window || document.body),
                viewTop = $w.scrollTop(),
                viewBottom = viewTop + $w.height(),
                _top = $t.offset().top - distance,
                _bottom = $t.offset().top + $t.height() + distance;
            // _left = $t.offset().left - distance,
            // _right = $t.offset().right - distance,

            return {
                top: _bottom <= viewTop,
                bottom: _top >= viewBottom,
            };
        };

        const win = $(window || document.body);
        const allModules = $('.lazy, .svg-bg');

        // all modules
        allModules.each((i, el) => {
            var el = $(el);
            if (!el.offScreen(150).bottom) {
                el.addClass('already-visible');
            }
        });

        win.on('scroll resize mousemove touchmove touchstart', (event) => {
            // for general loading
            allModules.each((i, el) => {
                var el = $(el);
                if (!el.offScreen(150).top && !el.offScreen(150).bottom) {
                    el.removeClass('already-visible off-screen-top off-screen-bottom');
                    el.addClass('come-in');
                } else {
                    if (el.offScreen(150).top) {
                        el.addClass('off-screen-top');
                    } else {
                        el.addClass('off-screen-bottom');
                    }
                }
            }); //allModules.each()
        }); //win.scroll()

        win.trigger('scroll');
    }

    loadSelectConvert() {
        $('.sidebar-container ul li a').each(function () {
            const el = $(this);
            $('<option />', {
                value: el.attr('href'),
                text: el.text(),
            }).appendTo('#sidebar-select select');
        });
        if ($('.sidebar-container ul li a').length < 1) $('#sidebar-select').hide();

        $('#sidebar-select')
            .find('select')
            .change(function () {
                window.location = $(this).find('option:selected').val();
            });
    }
}

export default new Base();
