/**
 * Creates a new class Base
 * @class Represents Base.
 */
class Mobile {
    /**
     * Initialises all delegates
     */
    initialise() {
        // show the file is loaded
        this.base();
        this.loadMobileMenu();
    }

    /**
     *  Check if the file is loaded
     */
    base() {
        console.log('%c Mobile JS has been found and loaded!', 'background: #03A9F4; color: #222222; padding: 5px; border-radius: 10px; font-weight: bold');
    }

    loadMobileMenu() {
        function isMobileDevice() {
            return typeof window.orientation !== 'undefined' || navigator.userAgent.indexOf('IEMobile') !== -1;
        }
        // if it's a mobile
        if (isMobileDevice()) {
            // get the li item with a hasDrop class
            let dropButton = $('header .navigate .toggle-js nav .menu .menu-main-container ul.menu li.hasDrop');
            console.log('drop button ', dropButton);

            // append a new button to the end
            dropButton.append('<span class="toggle-append-js"><span class="fa fa-angle-down"></span></span>');

            // get the new button
            let toggleTargetButton = dropButton.find('.toggle-append-js');
            console.log('toggleTargetButton ', toggleTargetButton);

            // find the parent, then target the drop menu
            let parentLink = toggleTargetButton.parent(dropButton).children('.csSubmenuWrap');
            console.log('parentLink ', parentLink);

            // add an initial class to the dropmenu of closed
            parentLink.addClass('closed');
            dropButton.addClass('closed');

            // on click, toggle classes
            toggleTargetButton.on('click', ({ currentTarget }) => {
                $(currentTarget).parent(dropButton).toggleClass('closed opened');
                $(currentTarget).parent(dropButton).children('.csSubmenuWrap').toggleClass('closed opened');
            });
        }
    }
}
export default new Mobile();
