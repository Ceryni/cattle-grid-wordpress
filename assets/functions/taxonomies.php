<?php

// Register Custom Taxonomy
function custom_taxonomy() {

    // Company Categories
    $labels = array(
        'name'                       => _x( 'Listing Categories', 'Taxonomy General Name', 'dysis' ),
        'singular_name'              => _x( 'Listing Category', 'Taxonomy Singular Name', 'dysis' ),
        'menu_name'                  => __( 'Listing Categories', 'dysis' ),
        'all_items'                  => __( 'All Categories', 'dysis' ),
        'parent_item'                => __( 'Parent Category', 'dysis' ),
        'parent_item_colon'          => __( 'Parent Category:', 'dysis' ),
        'new_item_name'              => __( 'New Category Name', 'dysis' ),
        'add_new_item'               => __( 'Add New Category', 'dysis' ),
        'edit_item'                  => __( 'Edit Category', 'dysis' ),
        'update_item'                => __( 'Update Category', 'dysis' ),
        'view_item'                  => __( 'View Category', 'dysis' ),
        'separate_items_with_commas' => __( 'Separate categories with commas', 'dysis' ),
        'add_or_remove_items'        => __( 'Add or remove categories', 'dysis' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'dysis' ),
        'popular_items'              => __( 'Popular categories', 'dysis' ),
        'search_items'               => __( 'Search Categories', 'dysis' ),
        'not_found'                  => __( 'Not Found', 'dysis' ),
        'no_terms'                   => __( 'No Categories', 'dysis' ),
        'items_list'                 => __( 'Categories list', 'dysis' ),
        'items_list_navigation'      => __( 'Categories list navigation', 'dysis' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'listing_categories', array( 'listings' ), $args );

    // Company Categories
    $labels = array(
        'name'                       => _x( 'Listing Locations', 'Taxonomy General Name', 'dysis' ),
        'singular_name'              => _x( 'Listing Location', 'Taxonomy Singular Name', 'dysis' ),
        'menu_name'                  => __( 'Listing Locations', 'dysis' ),
        'all_items'                  => __( 'All Locations', 'dysis' ),
        'parent_item'                => __( 'Parent Location', 'dysis' ),
        'parent_item_colon'          => __( 'Parent Location:', 'dysis' ),
        'new_item_name'              => __( 'New Location Name', 'dysis' ),
        'add_new_item'               => __( 'Add New Location', 'dysis' ),
        'edit_item'                  => __( 'Edit Location', 'dysis' ),
        'update_item'                => __( 'Update Location', 'dysis' ),
        'view_item'                  => __( 'View Location', 'dysis' ),
        'separate_items_with_commas' => __( 'Separate categories with commas', 'dysis' ),
        'add_or_remove_items'        => __( 'Add or remove categories', 'dysis' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'dysis' ),
        'popular_items'              => __( 'Popular categories', 'dysis' ),
        'search_items'               => __( 'Search Locations', 'dysis' ),
        'not_found'                  => __( 'Not Found', 'dysis' ),
        'no_terms'                   => __( 'No Locations', 'dysis' ),
        'items_list'                 => __( 'Locations list', 'dysis' ),
        'items_list_navigation'      => __( 'Locations list navigation', 'dysis' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'listing_locations', array( 'listings' ), $args );


    // Job Categories
    $labels = array(
        'name'                       => _x( 'Job Types', 'Taxonomy General Name', 'dysis' ),
        'singular_name'              => _x( 'Job Type', 'Taxonomy Singular Name', 'dysis' ),
        'menu_name'                  => __( 'Job Types', 'dysis' ),
        'all_items'                  => __( 'All Types', 'dysis' ),
        'parent_item'                => __( 'Parent Type', 'dysis' ),
        'parent_item_colon'          => __( 'Parent Type:', 'dysis' ),
        'new_item_name'              => __( 'New Type Name', 'dysis' ),
        'add_new_item'               => __( 'Add New Type', 'dysis' ),
        'edit_item'                  => __( 'Edit Type', 'dysis' ),
        'update_item'                => __( 'Update Type', 'dysis' ),
        'view_item'                  => __( 'View Type', 'dysis' ),
        'separate_items_with_commas' => __( 'Separate categories with commas', 'dysis' ),
        'add_or_remove_items'        => __( 'Add or remove categories', 'dysis' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'dysis' ),
        'popular_items'              => __( 'Popular categories', 'dysis' ),
        'search_items'               => __( 'Search Types', 'dysis' ),
        'not_found'                  => __( 'Not Found', 'dysis' ),
        'no_terms'                   => __( 'No Types', 'dysis' ),
        'items_list'                 => __( 'Type list', 'dysis' ),
        'items_list_navigation'      => __( 'Type list navigation', 'dysis' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'Job Types', array( 'jobs' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );


?>
