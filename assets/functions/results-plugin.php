<?php
/**
* Search & Filter Pro
*
* Sample Results Template
*
* @package   Search_Filter
* @author    Ross Morsali
* @link      http://www.designsandcode.com/
* @copyright 2015 Designs & Code
*
* Note: these templates are not full page templates, rather
* just an encaspulation of the your results loop which should
* be inserted in to other pages by using a shortcode - think
* of it as a template part
*
* This template is an absolute base example showing you what
* you can do, for more customisation see the WordPress docs
* and using template tags -
*
* http://codex.wordpress.org/Template_Tags
*
*/

if ( $query->have_posts() )
{
	?>

	<?php /*
	Found <?php echo $query->found_posts; ?> Results<br />
	Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />
	*/?>
	<div class="pagination">
		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
		/* example code for using the wp_pagenavi plugin */
		if (function_exists('wp_pagenavi'))
		{
			echo "<br />";
			wp_pagenavi( array( 'query' => $query ) );
		}
		?>
	</div>

	<?php
	while ($query->have_posts())
	{
		$query->the_post();

		?>
		<article itemscope itemtype="http://schema.org/Article" class="singlePost">
			<div class="row align-top align-center">
				<div class="col-12 col-md-3 col-lg-3">
					<?php if ( has_post_thumbnail() ) {
						the_post_thumbnail("thumbnail");
					};?>
				</div>
				<div class="col-12 col-md-9 col-lg-9">
					<h2><a href="<?php echo the_permalink();?>"><?php the_title();?></a></h2>
					<?php get_template_part('parts/templates/template', 'byline');?>
					<p itemprop="articleSection"><?php echo wp_trim_words( get_the_content(), 50 ); ?></p>
					<p><a class="readmore" href="<?php echo the_permalink();?>">Read More <span class="fa fa-angle-right"></span></a></p>
				</div>
			</div>
		</article>
		<?php
	}
	?>
	Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />

	<div class="pagination">

		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
		/* example code for using the wp_pagenavi plugin */
		if (function_exists('wp_pagenavi'))
		{
			echo "<br />";
			wp_pagenavi( array( 'query' => $query ) );
		}
		?>
	</div>
	<?php
}
else
{
	echo "No Results Found";
}
?>
