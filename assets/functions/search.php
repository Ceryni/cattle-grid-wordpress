<?php
add_filter( 'pre_get_posts', 'search_results' );
/**
* This function modifies the main WordPress query to include an array of
* post types instead of the default 'post' post type.
*
* @param object $query  The original query.
* @return object $query The amended query.
*/
function search_results( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', array( 'post', 'testimonials', 'dayinthelife', 'realnews', 'easyread' ) );
    }
    return $query;
}

function wps_highlight_results($text){
    if(is_search()){
        $sr = get_query_var('s');
        $keys = explode(" ",$sr);
        $text = preg_replace('/('.implode('|', $keys) .')/iu', '<strong class="search-excerpt">'.$sr.'</strong>', $text);
    }
    return $text;
}
add_filter('the_excerpt', 'wps_highlight_results');

function search_all( $query ) {
    if ( $query->is_search ) {
        $query->set( 'post_type', array('post','page','casestudies','service') );
    }
    return $query;
}
add_filter('pre_get_posts','search_all');
