<?php
/**
* page Slug as Body Class
*/
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );
/**
* Allow SVG's
*/
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
/**
* Allow shortcodes in custom taxonomies
*/
add_filter( 'term_description', 'do_shortcode' );
/**
* Changing excerpt more
*/
function new_excerpt_more($more) {
	global $post;
	remove_filter('excerpt_more', 'new_excerpt_more');
	return ' ...';
}
add_filter('excerpt_more','new_excerpt_more',11);
/**
* Filter except length
*/
function excerpt_length( $length ) {
	return 5000;
}
add_filter( 'excerpt_length', 'excerpt_length', 999 );
