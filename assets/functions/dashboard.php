<?php

// add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
//
//     function my_custom_dashboard_widgets() {
//     global $wp_meta_boxes;
//
//     wp_add_dashboard_widget('dash_welcome_widget', get_bloginfo( 'name' ), 'custom_dashboard_help');
// }
//
// function custom_dashboard_help() {
// echo '<p>Welcome to Custom Blog Theme! Need help? Contact the developer <a href="mailto:yourusername@gmail.com">here</a>. For WordPress Tutorials visit: <a href="https://www.wpbeginner.com" target="_blank">WPBeginner</a></p>';
// }


remove_action( 'welcome_panel', 'wp_welcome_panel' );
add_action( 'welcome_panel', 'my_custom_content' );

function my_custom_content()
{
    ?>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/cg-logo-light.png" />
    <h1>Welcome to your new site</h1>
    <p>We hope you enjoy the new <?php echo get_bloginfo( 'name' );?> build.</p>
    <em>
        - From <a href="https://ceryni.com" target="_blank">ceryni</a>
    </em>
    <?php
}

?>
