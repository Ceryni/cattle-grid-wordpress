<?php
// Register Custom Post Types
function post_type() {

	/**
	* Post Type: Case Studies.
	*/
	$labels = array(
		'name'                  => _x( 'Listings', 'Post Type General Name', 'castlepoint' ),
		'singular_name'         => _x( 'Listing', 'Post Type Singular Name', 'castlepoint' ),
		'menu_name'             => __( 'Listings', 'castlepoint' ),
		'name_admin_bar'        => __( 'Listings', 'castlepoint' ),
		'archives'              => __( 'Listings Archives', 'castlepoint' ),
		'attributes'            => __( 'Listings Attributes', 'castlepoint' ),
		'parent_item_colon'     => __( 'Parent Listing:', 'castlepoint' ),
		'all_items'             => __( 'Listings', 'castlepoint' ),
		'add_new_item'          => __( 'Add New Listing', 'castlepoint' ),
		'add_new'               => __( 'Add New Listing', 'castlepoint' ),
		'new_item'              => __( 'New Listing', 'castlepoint' ),
		'edit_item'             => __( 'Edit Listing', 'castlepoint' ),
		'update_item'           => __( 'Update Listing', 'castlepoint' ),
		'view_item'             => __( 'View Listing', 'castlepoint' ),
		'view_items'            => __( 'View Listings', 'castlepoint' ),
		'search_items'          => __( 'Search Listings', 'castlepoint' ),
		'not_found'             => __( 'Listings Not found', 'castlepoint' ),
		'not_found_in_trash'    => __( 'Listings Not found in Trash', 'castlepoint' ),
		'featured_image'        => __( 'Featured Image', 'castlepoint' ),
		'set_featured_image'    => __( 'Set featured image', 'castlepoint' ),
		'remove_featured_image' => __( 'Remove featured image', 'castlepoint' ),
		'use_featured_image'    => __( 'Use as featured image', 'castlepoint' ),
		'insert_into_item'      => __( 'Insert into Listings', 'castlepoint' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Listing', 'castlepoint' ),
		'items_list'            => __( 'Listings list', 'castlepoint' ),
		'items_list_navigation' => __( 'Listings list navigation', 'castlepoint' ),
		'filter_items_list'     => __( 'Filter Listings list', 'castlepoint' ),
	);
	$args = array(
		'label'                 => __( 'Listings', 'castlepoint' ),
		'description'           => __( 'Post Type Description', 'castlepoint' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', 'comments', 'page-attributes', 'post-formats' ),
		
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu' 			=> true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-archive',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'Listings', $args );


	/**
	* Post Type: Company
	*/
	$labels = array(
		'name'                  => _x( 'Event', 'Post Type General Name', 'castlepoint' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'castlepoint' ),
		'menu_name'             => __( 'Event', 'castlepoint' ),
		'name_admin_bar'        => __( 'Event', 'castlepoint' ),
		'archives'              => __( 'Event Archives', 'castlepoint' ),
		'attributes'            => __( 'Event Attributes', 'castlepoint' ),
		'parent_item_colon'     => __( 'Parent Event:', 'castlepoint' ),
		'all_items'             => __( 'Event', 'castlepoint' ),
		'add_new_item'          => __( 'Add New Event', 'castlepoint' ),
		'add_new'               => __( 'Add New Event', 'castlepoint' ),
		'new_item'              => __( 'New Event', 'castlepoint' ),
		'edit_item'             => __( 'Edit Event', 'castlepoint' ),
		'update_item'           => __( 'Update Event', 'castlepoint' ),
		'view_item'             => __( 'View Event', 'castlepoint' ),
		'view_items'            => __( 'View Event', 'castlepoint' ),
		'search_items'          => __( 'Search Events', 'castlepoint' ),
		'not_found'             => __( 'Event Not found', 'castlepoint' ),
		'not_found_in_trash'    => __( 'Event Not found in Trash', 'castlepoint' ),
		'featured_image'        => __( 'Featured Image', 'castlepoint' ),
		'set_featured_image'    => __( 'Set featured image', 'castlepoint' ),
		'remove_featured_image' => __( 'Remove featured image', 'castlepoint' ),
		'use_featured_image'    => __( 'Use as featured image', 'castlepoint' ),
		'insert_into_item'      => __( 'Insert into Event', 'castlepoint' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Case Study', 'castlepoint' ),
		'items_list'            => __( 'Event list', 'castlepoint' ),
		'items_list_navigation' => __( 'Event list navigation', 'castlepoint' ),
		'filter_items_list'     => __( 'Filter Events list', 'castlepoint' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'castlepoint' ),
		'description'           => __( 'Post Type Description', 'castlepoint' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', 'comments', 'page-attributes', 'post-formats' ),
		'hierarchical'          => true,

		'public'                => true,
		'show_ui'               => true,
		'show_in_menu' 			=> true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'Event', $args );

}
add_action( 'init', 'post_type');


//add an options page from acf
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	// Check function exists.
	if( function_exists('acf_add_options_page') ) {

		// Register options page.
		$option_page = acf_add_options_page(array(
			'page_title'    => __('Page Settings'),
			'menu_title'    => __('Page Settings'),
			'menu_slug'     => 'page-settings',
			'capability'    => 'edit_posts',
			'redirect'      => false,
			'parent_slug'    => 'edit.php?post_type=page',
		));
	}
}
