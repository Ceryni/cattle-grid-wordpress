<?php
/**
* Create & Style Related posts
*/
function c21_related_posts() {
	global $post;
	$tag_arr = '';
	$tags = wp_get_post_tags( $post->ID );
	if($tags) {
		foreach( $tags as $tag ) {
			$tag_arr .= $tag->slug . ',';
		}
		$args = array(
			'tag' => $tag_arr,
			'numberposts' => 3, /* you can change this to show more */
			'post__not_in' => array($post->ID)
		);
		$related_posts = get_posts( $args );
		if($related_posts) {
			echo __( '<h4>Related Posts</h4>', 'c21' );
			echo '<div class="row">';
			foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
			<div class="col-12 medium-4 columns">
				<div class="tile">
					<a href="<?php the_permalink(); ?>">
						<img src="<?php echo $image[0]; ?>">
					</a>
				</div>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<?php get_template_part('parts/templates/template', 'byline');?>
			</div>
		<?php endforeach; }
	}
	wp_reset_postdata();
	echo '</div>';
}
