<?php
function author_dropdown_list() {
    // query array
    $args = array(
        'query_id' => 'authors_with_posts',
    );
    $users = get_users($args);
    if( empty($users) )
    return;
    echo'<select>';
    echo '<option value="">Author</option>';
    foreach( $users as $user ){
        echo '<option value="'.$user->data->user_login.'">'.$user->data->display_name.'</option>';
    }
    echo'</select>';
}
