<?php
/**
* Register menus to theme
*/
register_nav_menus(
	array(
		'Menu Left' => 'Header Menu Left',
		'Menu Right' => 'Header Menu Right',
		'Menu Right Logged In' => 'Header Menu Right Logged In',
		'Main' => 'The Main Menu',
		'Footer Navigation' => 'Footer Navigation',
		'Sitemap' => 'Sitemap',
		'Top Level' => 'Top Level',
	)
);


/**
* Add active class to menu
*/
function required_active_nav_class( $classes, $item ) {
	if ( $item->current == 1 || $item->current_item_ancestor == true ) {
		$classes[] = 'active';
	}
	return $classes;
}
add_filter( 'nav_menu_css_class', 'required_active_nav_class', 10, 2 );


/**
* Add search box to primary menu
*/
// add_filter('wp_nav_menu_items','add_search_box', 10, 2);
// function add_search_box($items, $args) {
//
//         ob_start();
//         get_template_part('/parts/search/navigation-search');
//         $searchform = ob_get_contents();
//         ob_end_clean();
//
//         $items .= '<li class="searchItem">' . $searchform . '</li>';
//
//     return $items;
// }




function isMobile() {
	return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
