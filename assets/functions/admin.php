<?php
// This file handles the admin area and functions - You can use this file to make changes to the dashboard.
/************* DASHBOARD WIDGETS *****************/
/**
* Disable default dashboard widgets
*/
function disable_default_dashboard_widgets() {
	// Remove_meta_box('dashboard_right_now', 'dashboard', 'core');    // Right Now Widget
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');  // Incoming Links Widget
	remove_meta_box('dashboard_plugins', 'dashboard', 'core');         // Plugins Widget
	// Remove_meta_box('dashboard_quick_press', 'dashboard', 'core');  // Quick Press Widget
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');   // Recent Drafts Widget
	remove_meta_box('dashboard_primary', 'dashboard', 'core');         //
	remove_meta_box('dashboard_secondary', 'dashboard', 'core');       //
	// Removing plugin dashboard boxes
	remove_meta_box('yoast_db_widget', 'dashboard', 'normal');         // Yoast's SEO Plugin Widget
}
/**
* removing the dashboard widgets
*/
add_action('admin_menu', 'disable_default_dashboard_widgets');
/************* CUSTOMIZE ADMIN *******************/
/**
* Custom Backend Footer
*/
function c21_custom_admin_footer() {
	_e('<span id="footer-thankyou">Proudly built by <a href="http://c-21.co.uk/" target="_blank">C-21</a></span>.', 'c21');
}
/**
* adding it to the admin area
*/
add_filter('admin_footer_text', 'c21_custom_admin_footer');

// https://wpadmincolors.com/export/c21
function c21_admin_color_scheme() {
	//Get the theme directory
	$theme_dir = get_template_directory_uri();

	//C21
	wp_admin_css_color( 'c21', __( 'C21' ),
	$theme_dir . '/dist/theme/style.min.css',
	array( '#216079', '#f2f2f2', '#0080c6' , '#216079')
);
}
add_action('admin_init', 'c21_admin_color_scheme');


if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'hotlinks', 75, 75, true ); //(cropped)
}




add_filter('manage_edit-post_columns' , 'book_cpt_columns');
function book_cpt_columns($columns) {
    $new_columns = array(
        'featured' => __('Featured', 'ThemeName'),
    );
    return array_merge($columns, $new_columns);
}


function add_featured_check( $column ) {
    global $post;
    if ( 'featured' === $column ) {
        $active =  get_field( "featured_story", $post->ID );

		if($active == 1) {
			echo 'Featured';
		}
    }
}
add_action( 'manage_posts_custom_column', 'add_featured_check' );


function update_featured_post() {

	global $posts;
	global $post;

	// Get other post marked as featured
	$posts = get_posts([
		// Array of posts to check
		'post_type' => ['post'],
		'meta_key' => 'featured_story',
		'meta_value' => true,
		'post__not_in' => [$post->ID]
	]);

	// Remove previous featured posts
	if ( get_field( 'featured_story' ) ) {
		foreach( $posts as $p ) {
			update_field('featured_story', '0', $p->ID);
		}
	} return;

}
add_action('acf/save_post', 'update_featured_post', 20);


// allow for modules to be labled by title
add_filter('acf/fields/flexible_content/layout_title/name=patient_modules', 'my_acf_fields_flexible_content_layout_title', 10, 4);
function my_acf_fields_flexible_content_layout_title( $title, $field, $layout, $i ) {

    // get the field
    $original = $field;
    // get the field name
    $step = $original['value'][$i]['acf_fc_layout'];
    // strip it of the underscores
    $step = str_replace('_', ' ', ucwords($step));
    // change row to ROW

    // Display thumbnail image.
    if( $image = get_sub_field('image') ) {
        $title .= '<div class="thumbnail"><img src="' . esc_url($image['sizes']['thumbnail']) . '" height="36px" /></div>';
    }

    // load text sub field
    if( $text = get_sub_field('admin_title') ) {
        $title = '<b> Module:</b> ' . esc_html($text) . ' <span class="original-title">[' . esc_html($step) . ']</span>';
    }
    return $title;
}
