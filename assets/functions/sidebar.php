<?php
/**
* Register Sidebars
*/
function c21_register_sidebars() {
	register_sidebar(array(
		'id' => 'Blog',
		'name' => __('Blog', 'c21'),
		'description' => __('The first (primary) sidebar.', 'c21'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
}
