<?php
/**
 * Adding WP Functions & Theme Support
 */
function c21_theme_support() {
	// Add WP Thumbnail Support
	add_theme_support( 'post-thumbnails' );
	// Default thumbnail size
	set_post_thumbnail_size(125, 125, true);
	// Add RSS Support
	add_theme_support( 'automatic-feed-links' );
	// Add Support for WP Controlled Title Tag
	add_theme_support( 'title-tag' );
	add_theme_support( "custom-header");
	add_theme_support( "custom-background" );
	// Add HTML5 Support
	add_theme_support( 'html5',
	         array(
	         	'comment-list',
	         	'comment-form',
	         	'search-form',
	         )
	);
	// Set the maximum allowed width for any content in the theme, like oEmbeds and images added to posts.
	$GLOBALS['content_width'] = apply_filters( 'c21_theme_support', 1200 );
} /* end theme support */
add_action( 'after_setup_theme', 'c21_theme_support' );


function redirect_staff_resources() {
    if(is_page ('resources', 'resource-gallery') && !is_user_logged_in ()) {
        $loginUrl = home_url('/staff-login');
        wp_redirect($loginUrl);
        exit();
    }
}
add_action( 'template_redirect', 'redirect_staff_resources' );

// add_action('init', 'init_remove_support',100);
// function init_remove_support(){
//     remove_post_type_support( 'page', 'editor');
// }


add_action( 'init', 'stop_heartbeat', 1 );
function stop_heartbeat() {
	wp_deregister_script('heartbeat');
}


// show featured images in dashboard
add_image_size( 'haizdesign-admin-post-featured-image', 120, 120, false );

// Add the posts and pages columns filter. They both use the same function.
add_filter('manage_posts_columns', 'haizdesign_add_post_admin_thumbnail_column', 2);
add_filter('manage_pages_columns', 'haizdesign_add_post_admin_thumbnail_column', 2);

// Add the column
function haizdesign_add_post_admin_thumbnail_column($haizdesign_columns){
    $haizdesign_columns['haizdesign_thumb'] = __('Featured Image');
    return $haizdesign_columns;
}

// Manage Post and Page Admin Panel Columns
add_action('manage_posts_custom_column', 'haizdesign_show_post_thumbnail_column', 5, 2);
add_action('manage_pages_custom_column', 'haizdesign_show_post_thumbnail_column', 5, 2);

// Get featured-thumbnail size post thumbnail and display it
function haizdesign_show_post_thumbnail_column($haizdesign_columns, $haizdesign_id){
    switch($haizdesign_columns){
        case 'haizdesign_thumb':
        if( function_exists('the_post_thumbnail') ) {
            echo the_post_thumbnail( 'haizdesign-admin-post-featured-image' );
        }
        else
            echo 'hmm… your theme doesn\'t support featured image…';
        break;
    }
}


add_filter('cf7_2_post_filter-contact-form-1-slug','filter_contact_form_1_slug',10,3);
function filter_contact_form_1_slug($value, $post_id, $form_data){
  //$value is the post field value to return, by default it is empty. If you are filtering a taxonomy you can return either slug/id/array.  in case of ids make sure to cast them as integers.(see https://codex.wordpress.org/Function_Reference/wp_set_object_terms for more information.)
  //$post_id is the ID of the post to which the form values are being mapped to
  // $form_data is the submitted form data as an array of field-name=>value pairs
  return $value;
}

add_filter( 'cf7_2_post_status_listings', 'publish_new_listings',10,3);
  /**
  * Function to change the post status of saved/submitted posts.
  * @param string $status the post status, default is 'draft'.
  * @param string $ckf7_key unique key to identify your form.
  * @param array $submitted_data complete set of data submitted in the form as an array of field-name=>value pairs.
  * @return string a valid post status ('publish'|'draft'|'pending'|'trash')
  */
  function publish_new_listings($status, $ckf7_key, $submitted_data){
    /*The default behaviour is to save post to 'draft' status.  If you wish to change this, you can use this filter and return a valid post status: 'publish'|'draft'|'pending'|'trash'*/
    return 'publish';
  }
