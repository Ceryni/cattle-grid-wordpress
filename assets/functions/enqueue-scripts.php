<?php

// fe styles and scripts
function site_scripts() {
	// bring in the caching timings
	$date   = new DateTime(); // returns the current date time
	$result = $date->format('Y-m-d-H-i-s');
	$krr    = explode('-', $result);
	$result = implode("", $krr);
	global $wp_styles;
	global $post;

	wp_deregister_script('jquery');
	wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js', null, null, false );

	// custom styles and scripts
	wp_enqueue_script('site-js', get_template_directory_uri() . '/dist/main/base.min.js?v='.$result.'', array( 'jquery' ), $result, true );
	wp_enqueue_style( 'site-css', get_template_directory_uri() . '/dist/main/style.min.css?v='.$result.'', array() );

	// external libs
	wp_enqueue_style( 'typekit', 'https://use.typekit.net/dwi4lig.css', false );
	wp_enqueue_style( 'fancybox-css', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.css');


	wp_enqueue_script('font-js', 'https://fonts.googleapis.com');
	wp_enqueue_script('font-1-js', 'https://fonts.gstatic.com');
	wp_enqueue_style( 'font-css', 'https://fonts.googleapis.com/css2?family=Goudy+Bookletter+1911&family=Quicksand:wght@300;400;600&display=swap', false );

	// Comment reply script for threaded comments
	if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);

// admin styles
function admin_style() {
	// bring in the caching timings
	$date   = new DateTime(); // returns the current date time
	$result = $date->format('Y-m-d-H-i-s');
	$krr    = explode('-', $result);
	$result = implode("", $krr);
	global $wp_styles;

	wp_enqueue_style('admin-styles', get_template_directory_uri() . '/dist/admin/style.min.css?v='.$result.'', array() );
	wp_enqueue_script('jquery-ui-widget');

	wp_enqueue_script('font-js', 'https://fonts.googleapis.com');
	wp_enqueue_script('font-1-js', 'https://fonts.gstatic.com');
	wp_enqueue_style( 'font-css', 'https://fonts.googleapis.com/css2?family=Goudy+Bookletter+1911&family=Quicksand:wght@300;400;600&display=swap', false );
}
add_action('admin_enqueue_scripts', 'admin_style', 1);
