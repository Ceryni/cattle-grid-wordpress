<?php
/**
* Calling your own login css so you can style it
*/
function c21_login_css() {
	wp_enqueue_style( 'c21_login_css', get_template_directory_uri() . '/dist/login/style.min.css' );
}
/**
* Changing the logo link from wordpress.org to your site
*/
function c21_login_url() {
	return home_url();
}
/**
* Changing the alt text on the logo to show your site name
*/
function c21_login_title() {
	return get_option('blogname');
}
/**
* Calling it only on the login page
*/
add_action( 'login_enqueue_scripts', 'c21_login_css', 10 );
add_filter('login_headerurl', 'c21_login_url');
add_filter('login_headertitle', 'c21_login_title');


// redirect users
function custom_login_redirect( $redirect_to, $request, $user ) {
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		if ( in_array( 'administrator', $user->roles )) {
			$redirect_to = admin_url();
		} else {
			$redirect_to = home_url( '/staff-login/resources/' );
		}
	}
	return $redirect_to;
}
add_filter( 'login_redirect', 'custom_login_redirect', 10, 3 );
