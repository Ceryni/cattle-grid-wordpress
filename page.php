<?php

/**
 * Template Name: Standard Page
 */
	get_header();
?>

<?php get_template_part('parts/components/component', 'hero');?>

<?php get_template_part('parts/components/component', 'breadcrumb');?>


<section class="component-centered-text">
	<div class="row align-center align-middle">
		<div class="col-12 col-lg-10 text-center lazy">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
			echo '<h1>' . get_the_title() . '</h1>';
			echo '<p>' . get_the_content() . '</p>';
				endwhile; else: ?>
			<?php endif; ?>
		</div>
	</div>
</section>


<?php get_template_part('parts/modules/modules', 'all');?>

<?php get_template_part('parts/templates/template', 'footer');?>
